package com.fablesoft.projectdatacollect.ui;

import com.fablesoft.projectdatacollect.R;
import com.fablesoft.projectdatacollect.util.CodeBitmap;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

public class QRCodeActivity extends BaseRequestActivity {

    @Override protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_erweima);
        initView();
    }

    private void initView(){
    	View titleBack = getTitleBack();
    	titleBack.setOnClickListener(mClickListener);
    	titleBack.setVisibility(View.VISIBLE);
    	String content = getIntent().getStringExtra("content");
    	CodeBitmap codeBitmap = new CodeBitmap();
    	Bitmap bitmap = codeBitmap.createBitmap(content, 600, 600);
    	ImageView erweimaView = (ImageView) findViewById(R.id.imageview);
    	erweimaView.setImageBitmap(bitmap);
    }
    
    private OnClickListener mClickListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.title_back:
				finish();
				break;
			}
		}
	};
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
	}
}
