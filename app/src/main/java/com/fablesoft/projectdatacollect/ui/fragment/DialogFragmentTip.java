package com.fablesoft.projectdatacollect.ui.fragment;

import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.fablesoft.projectdatacollect.R;
import com.fablesoft.projectdatacollect.ui.RegisteredSearchActivity;
import com.fablesoft.projectdatacollect.ui.SignFingerprintActivity;

public class DialogFragmentTip extends DialogFragment implements View.OnClickListener {
    private View view;
    private ImageView ivCancel;
    private TextView tvRegistered;
    private TextView tvNotRegister;
    private String id;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        id = getArguments().getString("id");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        view = inflater.inflate(R.layout.dialog_fingerprint_tip, null);

        ivCancel = (ImageView) view.findViewById(R.id.iv_cancel);
        tvNotRegister = (TextView) view.findViewById(R.id.bt_not_register);
        tvRegistered = (TextView) view.findViewById(R.id.bt_registered);

        ivCancel.setOnClickListener(this);
        tvNotRegister.setOnClickListener(this);
        tvRegistered.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_cancel:
                dismiss();
                break;
            case R.id.bt_not_register:
                jumpToSign();
                break;
            case R.id.bt_registered:
                jumpToSearch();
                break;
        }
    }

    public void jumpToSearch(){
        Intent intent = new Intent(getActivity(), RegisteredSearchActivity.class);
        intent.putExtra("id", id);
        startActivity(intent);
        dismiss();
    }

    public void jumpToSign(){
        Intent intent = new Intent(getActivity(), SignFingerprintActivity.class);
        Bundle bundle = new Bundle();
        bundle.putBoolean("isRegister", false);
        bundle.putString("id", id);
        intent.putExtra("data", bundle);
        startActivity(intent);
        dismiss();
    }
}
