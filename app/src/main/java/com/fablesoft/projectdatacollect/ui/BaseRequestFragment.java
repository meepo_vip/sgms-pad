package com.fablesoft.projectdatacollect.ui;

import java.util.Map;

import com.fablesoft.projectdatacollect.MyApplication;
import com.fablesoft.projectdatacollect.R;
import com.fablesoft.projectdatacollect.bean.BaseResponse;
import com.fablesoft.projectdatacollect.http.HttpListener;
import com.fablesoft.projectdatacollect.http.HttpThread;
import com.fablesoft.projectdatacollect.view.MyProgressDialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class BaseRequestFragment extends Fragment implements HttpListener{

	private MyProgressDialog mProgressDialog;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

    protected void sendRequest(String url, Map<String, Object> params, Class clazz){
    	HttpThread httpThread = new HttpThread(url, this, params, clazz);
    	MyApplication.getInstance().getRequestQueue().execute(httpThread);
    }
	
    /**
     * HTTP请求处理成功时调用该方法
     * 
     * @param data data
     */
    @Override
	public void onRequestFinish(BaseResponse response){
    	
    };
    
    protected void createLoadingDialog(Context mContext) {
    	if(mProgressDialog == null){
    		mProgressDialog = new MyProgressDialog(mContext, "");
    	}
	}
	
    protected void createPromptDialog(Activity mContext, String title,
			String content, String confirmStr, String cancelStr,
			final ConfirmClickListener confirmListener) {

		final Dialog dialog = new Dialog(mContext,
				R.style.Theme_Light_CustomDialog);
		dialog.setContentView(R.layout.prompt_dialog_layout);
		dialog.setCanceledOnTouchOutside(false);
		WindowManager m = mContext.getWindowManager();
		Display d = m.getDefaultDisplay(); // 获取屏幕宽、高用
		WindowManager.LayoutParams p = dialog.getWindow().getAttributes(); // 获取对话框当前的参数值
		p.width = (int) (d.getWidth() * 0.8); // 宽度设置为屏幕的0.8
		dialog.getWindow().setAttributes(p);
		TextView promptTitle = (TextView) dialog
				.findViewById(R.id.prompt_title);
		TextView promptContent = (TextView) dialog
				.findViewById(R.id.prompt_content);
		TextView confirmBtn = (TextView) dialog.findViewById(R.id.confirm_btn);
		TextView cancelBtn = (TextView) dialog.findViewById(R.id.cancel_btn);
		if (!TextUtils.isEmpty(title)) {
			promptTitle.setText(title);
		}
		if (!TextUtils.isEmpty(content)) {
			promptContent.setText(content);
		}
		if (!TextUtils.isEmpty(confirmStr)) {
			confirmBtn.setText(confirmStr);
		}
		if (!TextUtils.isEmpty(cancelStr)) {
			cancelBtn.setText(cancelStr);
		}
		OnClickListener dialogOnClickListener = new OnClickListener() {
			@Override
			public void onClick(View v) {
				switch (v.getId()) {
				case R.id.cancel_btn:
					dialog.dismiss();
					break;

				case R.id.confirm_btn:
					if(confirmListener != null){
						confirmListener.confirmClickImp();
					}
					dialog.dismiss();
					break;
				}
			}
		};
		cancelBtn.setOnClickListener(dialogOnClickListener);
		confirmBtn.setOnClickListener(dialogOnClickListener);
		dialog.show();
	}

	protected void createPromptDialog(Activity mContext, String title,
			String content, String confirmStr, final ConfirmClickListener confirmListener) {

		final Dialog dialog = new Dialog(mContext,
				R.style.Theme_Light_CustomDialog);
		dialog.setContentView(R.layout.confirm_prompt_dialog_layout);
		dialog.setCanceledOnTouchOutside(false);
		WindowManager m = mContext.getWindowManager();
		Display d = m.getDefaultDisplay(); // 获取屏幕宽、高用
		WindowManager.LayoutParams p = dialog.getWindow().getAttributes(); // 获取对话框当前的参数值
		p.width = (int) (d.getWidth() * 0.8); // 宽度设置为屏幕的8
		dialog.getWindow().setAttributes(p);
		TextView promptTitle = (TextView) dialog
				.findViewById(R.id.prompt_title);
		TextView promptContent = (TextView) dialog
				.findViewById(R.id.prompt_content);
		TextView confirmBtn = (TextView) dialog.findViewById(R.id.confirm_btn);
		if (!TextUtils.isEmpty(title)) {
			promptTitle.setText(title);
		}
		if (!TextUtils.isEmpty(content)) {
			promptContent.setText(content);
		}
		if (!TextUtils.isEmpty(confirmStr)) {
			confirmBtn.setText(confirmStr);
		}
		OnClickListener dialogOnClickListener = new OnClickListener() {
			@Override
			public void onClick(View v) {
				switch (v.getId()) {
				case R.id.cancel_btn:
					dialog.dismiss();
					break;

				case R.id.confirm_btn:
					if(confirmListener != null){
						confirmListener.confirmClickImp();
					}
					dialog.dismiss();
					break;
				}
			}
		};
		confirmBtn.setOnClickListener(dialogOnClickListener);
		dialog.show();
	}
	
    protected interface ConfirmClickListener{
    	void confirmClickImp();
    }
	/**
	 * 显示dialog
	 */
	public void showDialog(String content) {
		if (mProgressDialog != null) {
			mProgressDialog.setPressText(content);
			if (!mProgressDialog.isShowing()) {
			    mProgressDialog.show();
			}
		}
	}

	/**
	 * 关闭dialog
	 */
	public void dismissDialog() {
		if (mProgressDialog != null) {
			if (mProgressDialog.isShowing()) {
			    mProgressDialog.dismiss();
			}
		}
	}
	
	@Override
	public void onNotLogin(BaseResponse response) {
		goLoginActivity();
	}
	
	private void goLoginActivity(){
		MyApplication.getInstance().clearUserInfo();
        Intent intent = new Intent(getActivity(), LoginActivity.class);
		intent.putExtra("loginTimeOut", true);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP );
		startActivity(intent);
	}
}
