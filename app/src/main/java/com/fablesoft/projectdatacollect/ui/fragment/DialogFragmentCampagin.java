package com.fablesoft.projectdatacollect.ui.fragment;

import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.fablesoft.projectdatacollect.R;
import com.fablesoft.projectdatacollect.view.DividerGridItemDecoration;

public class DialogFragmentCampagin extends DialogFragment implements View.OnClickListener {
    private View view;
    private RecyclerView rvBaseInfo;
    private TextView tvTitle;
    private ImageView ivCancel;

    private String[] datas;
    private String title;
    private ItemCheckedListener listener;
    private Context context;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        view = inflater.inflate(R.layout.dialog_baseinfo_selector, null);

        tvTitle = (TextView) view.findViewById(R.id.tv_title);
        tvTitle.setText(title);
        ivCancel = (ImageView) view.findViewById(R.id.iv_cancel);
        ivCancel.setOnClickListener(this);

        rvBaseInfo = (RecyclerView) view.findViewById(R.id.rv_baseinfo);
        rvBaseInfo.setLayoutManager(new GridLayoutManager(context, 3));
        rvBaseInfo.setAdapter(new HomeAdapter());
        rvBaseInfo.addItemDecoration(new DividerGridItemDecoration(context));

        return view;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setItemCheckedListener(ItemCheckedListener listener) {
        this.listener = listener;
    }

    public void setDatas(String[] datas) {
        this.datas = datas;
    }

    class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.MyViewHolder> {

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            MyViewHolder holder = new MyViewHolder(LayoutInflater.from(getActivity()).inflate(R.layout.item_baseinfo, parent,
                    false));
            return holder;
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, final int position) {
            holder.tv.setText(datas[position]);
            holder.tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                    listener.onChecked(position);
                }
            });
        }

        @Override
        public int getItemCount() {
            return datas.length;
        }

        class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tv;

            public MyViewHolder(View view) {
                super(view);
                tv = (TextView) view.findViewById(R.id.tv_menu);
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_cancel:
                dismiss();
                break;
        }
    }

    public interface ItemCheckedListener {
        void onChecked(int position);
    }
}
