package com.fablesoft.projectdatacollect.ui;

import com.fablesoft.projectdatacollect.R;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class SaomaResultActivity extends BaseRequestActivity {

    @Override protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saomaresult);
        init();
    }

    private void init(){
    	View titleBack = getTitleBack();
    	titleBack.setOnClickListener(mClickListener);
    	titleBack.setVisibility(View.VISIBLE);
    	getTitleName().setText("扫描结果");
    	TextView resultText = (TextView) findViewById(R.id.result_text);
    	String result = getIntent().getStringExtra("result");
    	resultText.setText(result);
    }
    	
    
    private OnClickListener mClickListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.title_back:
				finish();
				break;
			}
		}
	};
}
