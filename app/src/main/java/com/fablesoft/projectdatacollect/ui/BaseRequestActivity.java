package com.fablesoft.projectdatacollect.ui;

import java.io.File;
import java.util.List;
import java.util.Map;

import com.authentication.utils.ToastUtil;
import com.fablesoft.projectdatacollect.MyApplication;
import com.fablesoft.projectdatacollect.R;
import com.fablesoft.projectdatacollect.bean.BaseResponse;
import com.fablesoft.projectdatacollect.http.HttpListener;
import com.fablesoft.projectdatacollect.http.HttpThread;
import com.fablesoft.projectdatacollect.http.HttpUploadThread;
import com.fablesoft.projectdatacollect.view.MyProgressDialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.TextView;

import android_serialport_api.SerialPortManager;

public class BaseRequestActivity extends BaseActivity implements HttpListener {

	private MyProgressDialog mProgressDialog;
	
	private Dialog mDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	protected void sendRequest(String url, Map<String, Object> params,
			Class clazz) {
		HttpThread httpThread = new HttpThread(url, this, params, clazz);
		MyApplication.getInstance().getRequestQueue().execute(httpThread);
	}
	
	protected void sendGetRequest(String url,Class clazz) {
		HttpThread httpThread = new HttpThread(url, this, clazz, true);
		MyApplication.getInstance().getRequestQueue().execute(httpThread);
	}
	
	protected void sendImageRequest(String url, List<File> files,
			Map<String, Object> params, Class clazz) {
		HttpUploadThread httpThread = new HttpUploadThread(url, this, files,
				params, clazz);
		MyApplication.getInstance().getRequestQueue().execute(httpThread);
	}

	/**
	 * HTTP请求处理成功时调用该方法
	 * 
	 * @param data
	 *            data
	 */
	@Override
	public void onRequestFinish(BaseResponse response) {

	}
	public void createLoadingDialog(Context mContext) {
		if (mProgressDialog == null) {
			mProgressDialog = new MyProgressDialog(mContext, "");
		}
	}

	protected interface ConfirmClickListener {
		void confirmClickImp();
	}
	
	protected void createPromptDialog(Activity mContext, String title,
			String content, String confirmStr, String cancelStr,
			final ConfirmClickListener confirmListener) {
		if(mDialog == null){
			mDialog = new Dialog(mContext,
					R.style.Theme_Light_CustomDialog);
			mDialog.setContentView(R.layout.prompt_dialog_layout);
			mDialog.setCanceledOnTouchOutside(false);
			WindowManager m = mContext.getWindowManager();
			Display d = m.getDefaultDisplay(); // 获取屏幕宽、高用
			WindowManager.LayoutParams p = mDialog.getWindow().getAttributes(); // 获取对话框当前的参数值
			p.width = (int) (d.getWidth() * 0.4); // 宽度设置为屏幕的0.8
			mDialog.getWindow().setAttributes(p);
		}
		TextView promptTitle = (TextView) mDialog
				.findViewById(R.id.prompt_title);
		TextView promptContent = (TextView) mDialog
				.findViewById(R.id.prompt_content);
		TextView confirmBtn = (TextView) mDialog.findViewById(R.id.confirm_btn);
		TextView cancelBtn = (TextView) mDialog.findViewById(R.id.cancel_btn);
		if (!TextUtils.isEmpty(title)) {
			promptTitle.setText(title);
		}
		if (!TextUtils.isEmpty(content)) {
			promptContent.setText(content);
		}
		if (!TextUtils.isEmpty(confirmStr)) {
			confirmBtn.setText(confirmStr);
		}
		if (!TextUtils.isEmpty(cancelStr)) {
			cancelBtn.setText(cancelStr);
		}
		OnClickListener dialogOnClickListener = new OnClickListener() {
			@Override
			public void onClick(View v) {
				switch (v.getId()) {
				case R.id.cancel_btn:
					mDialog.dismiss();
					break;

				case R.id.confirm_btn:
					if(confirmListener != null){
						confirmListener.confirmClickImp();
					}
					mDialog.dismiss();
					break;
				}
			}
		};
		cancelBtn.setOnClickListener(dialogOnClickListener);
		confirmBtn.setOnClickListener(dialogOnClickListener);
		mDialog.show();
	}

	protected void createPromptDialog(Activity mContext, String title,
			String content, String confirmStr, final ConfirmClickListener confirmListener) {
		if(mDialog == null){
			mDialog = new Dialog(mContext,
					R.style.Theme_Light_CustomDialog);
			mDialog.setContentView(R.layout.confirm_prompt_dialog_layout);
			mDialog.setCanceledOnTouchOutside(false);
			WindowManager m = mContext.getWindowManager();
			Display d = m.getDefaultDisplay(); // 获取屏幕宽、高用
			WindowManager.LayoutParams p = mDialog.getWindow().getAttributes(); // 获取对话框当前的参数值
			p.width = (int) (d.getWidth() * 0.4); // 宽度设置为屏幕的8
			mDialog.getWindow().setAttributes(p);
		}
		TextView promptTitle = (TextView) mDialog
				.findViewById(R.id.prompt_title);
		TextView promptContent = (TextView) mDialog
				.findViewById(R.id.prompt_content);
		TextView confirmBtn = (TextView) mDialog.findViewById(R.id.confirm_btn);
		if (!TextUtils.isEmpty(title)) {
			promptTitle.setText(title);
		}
		if (!TextUtils.isEmpty(content)) {
			promptContent.setText(content);
		}
		if (!TextUtils.isEmpty(confirmStr)) {
			confirmBtn.setText(confirmStr);
		}
		OnClickListener dialogOnClickListener = new OnClickListener() {
			@Override
			public void onClick(View v) {
				switch (v.getId()) {
				case R.id.cancel_btn:
					mDialog.dismiss();
					break;

				case R.id.confirm_btn:
					if(confirmListener != null){
						confirmListener.confirmClickImp();
					}
					mDialog.dismiss();
					break;
				}
			}
		};
		confirmBtn.setOnClickListener(dialogOnClickListener);
		mDialog.show();
	}
	
	@Override
	protected void onDestroy() {
		if(mDialog != null){
			mDialog.dismiss();
		}
		super.onDestroy();
	}
	
	/**
	 * 显示dialog
	 */
	public void showDialog(String content) {
		if (mProgressDialog != null) {
			mProgressDialog.setPressText(content);
			if (!mProgressDialog.isShowing()) {
				mProgressDialog.show();
			}
		}
	}

	/**
	 * 关闭dialog
	 */
	public void dismissDialog() {
		if (mProgressDialog != null) {
			if (mProgressDialog.isShowing()) {
				mProgressDialog.dismiss();
			}
		}
	}

	@Override
	public void onNotLogin(BaseResponse response) {
		goLoginActivity();
	}
	
	public void goLoginActivity(){
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				dismissDialog();
			}
		} );
		MyApplication.getInstance().clearUserInfo();
        Intent intent = new Intent(this, LoginActivity.class);
		intent.putExtra("loginTimeOut", true);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP );
		startActivity(intent);
	}

	protected void openSerialPort(){
		if (!SerialPortManager.getInstance().isOpen()
				&& !SerialPortManager.getInstance().openSerialPort()) {
			ToastUtil.showToast(this, R.string.open_serial_fail);
		}
	}
}
