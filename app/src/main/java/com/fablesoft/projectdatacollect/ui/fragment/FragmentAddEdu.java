package com.fablesoft.projectdatacollect.ui.fragment;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.authentication.utils.ToastUtil;
import com.fablesoft.projectdatacollect.MyApplication;
import com.fablesoft.projectdatacollect.R;
import com.fablesoft.projectdatacollect.bean.BaseResponse;
import com.fablesoft.projectdatacollect.http.HttpListener;
import com.fablesoft.projectdatacollect.http.HttpThread;
import com.fablesoft.projectdatacollect.ui.SafeCampaignNewActivity;
import com.fablesoft.projectdatacollect.util.Config;
import com.fablesoft.projectdatacollect.util.DateUtil;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class FragmentAddEdu extends Fragment implements View.OnClickListener {
    private View view;
    private String[] types;
    private String[] periods;
    private TextView tvType, tvTime, tvPeriod;
    private EditText etName, etOrg, etPosition, etHost, etRecorder;
    private final int ADD_CODE = 100;
    private Context context;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_add_edu, container, false);
        initData();
        initView();
        return view;
    }

    private void initData() {
        context = getActivity();
        types = getResources().getStringArray(R.array.campaign_edu_type);
        periods = getResources().getStringArray(R.array.campaign_period);
    }

    private void initView() {
        tvType = (TextView) view.findViewById(R.id.tv_type);
        tvTime = (TextView) view.findViewById(R.id.tv_time);
        tvPeriod = (TextView) view.findViewById(R.id.tv_period);
        etName = (EditText) view.findViewById(R.id.et_name);
        etOrg = (EditText) view.findViewById(R.id.et_org);
        etPosition = (EditText) view.findViewById(R.id.et_position);
        etHost = (EditText) view.findViewById(R.id.et_host);
        etRecorder = (EditText) view.findViewById(R.id.et_recorder);

        tvTime.setText(DateUtil.getCurrentDateStr());
        tvType.setOnClickListener(this);
        tvTime.setOnClickListener(this);
        tvPeriod.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        DialogFragmentCampagin dialog;
        switch (v.getId()) {
            case R.id.tv_type:
                dialog = new DialogFragmentCampagin();
                dialog.setTitle("请选择培训方式");
                dialog.setDatas(types);
                dialog.setItemCheckedListener(new DialogFragmentCampagin.ItemCheckedListener() {
                    @Override
                    public void onChecked(int position) {
                        tvType.setText(types[position]);
                        tvType.setTag(position + 7);
                    }
                });
                dialog.show(getFragmentManager(), null);
                break;
            case R.id.tv_period:
                dialog = new DialogFragmentCampagin();
                dialog.setTitle("请选择学时");
                dialog.setDatas(periods);
                dialog.setItemCheckedListener(new DialogFragmentCampagin.ItemCheckedListener() {
                    @Override
                    public void onChecked(int position) {
                        tvPeriod.setText(periods[position]);
                        tvPeriod.setTag(position + 7);
                    }
                });
                dialog.show(getFragmentManager(), null);
                break;
            case R.id.tv_time:
                showDatePickerDialog();
                break;
        }
    }


    private void showDatePickerDialog() {
        Calendar calendar = Calendar.getInstance();
        DatePickerDialog dialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                tvTime.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
            }
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        dialog.show();
    }

    public void commit() {
        String name = etName.getText().toString();
        String org = etOrg.getText().toString();
        String position = etPosition.getText().toString();
        String host = etHost.getText().toString();
        String type = tvType.getText().toString();
        String period = tvPeriod.getText().toString();
        String time = tvTime.getText().toString();
        if (TextUtils.isEmpty(name)) {
            ToastUtil.showToast(context, "请填写培训主题");
            return;
        }
        if (TextUtils.isEmpty(org)) {
            ToastUtil.showToast(context, "请填写组织部门");
            return;
        }
        if (TextUtils.isEmpty(position)) {
            ToastUtil.showToast(context, "请填写培训地点");
            return;
        }
        if (TextUtils.isEmpty(host)) {
            ToastUtil.showToast(context, "请填写主持人");
            return;
        }
        if (TextUtils.isEmpty(type)) {
            ToastUtil.showToast(context, "请选择培训方式");
            return;
        }
        if (TextUtils.isEmpty(period)) {
            ToastUtil.showToast(context, "请选择学时");
            return;
        }
        if (TextUtils.isEmpty(time)) {
            ToastUtil.showToast(context, "请选择培训时间");
            return;
        }

        Map<String, Object> params = new HashMap<>();
        params.put("name", name);
        params.put("content", "");
        params.put("organizer", org);
        params.put("site", position);
        params.put("speaker", host);
        params.put("technicalType", tvType.getTag());
        params.put("recorder", TextUtils.isEmpty(etRecorder.getText()) ? "" : etRecorder.getText().toString());
        params.put("startTime", time);
        params.put("period", period.substring(0, period.length() - 2));
        HttpThread thread = new HttpThread(Config.ADD_CAMPAGIN_EDU,
                new HttpListener() {
                    @Override
                    public void onRequestFinish(BaseResponse response) {
                        Message msg = mHandler.obtainMessage();
                        msg.obj = response;
                        msg.what = ADD_CODE;
                        mHandler.sendMessage(msg);
                    }

                    @Override
                    public void onNotLogin(BaseResponse response) {
                    }
                }, params, BaseResponse.class);
        MyApplication.getInstance().getRequestQueue().execute(thread);
    }

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case ADD_CODE:
                    BaseResponse result = (BaseResponse) msg.obj;
                    if (result == null) {
                        Toast.makeText(context, "请检查网络或服务器请求路径", Toast.LENGTH_SHORT).show();
                    } else if (!result.getSuccess()) {
                        Toast.makeText(context, "添加失败，" + result.getMessage(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, "添加成功", Toast.LENGTH_SHORT).show();
                        ((Activity) context).setResult(SafeCampaignNewActivity.UPDATE_LIST_CODE);
                        ((Activity) context).finish();
                    }
                    break;
            }
        }
    };
}
