package com.fablesoft.projectdatacollect.ui;

import com.fablesoft.projectdatacollect.R;
import com.fablesoft.projectdatacollect.util.CommonUtils;
import com.fablesoft.projectdatacollect.util.Log4jUtil;

import android.app.Activity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class BaseActivity extends Activity{
    private static final long CLICK_REPEATTIME = 500;
    private long mLastClickTime = 0;
    private int mLastClickViewId = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log4jUtil.init();
		super.onCreate(savedInstanceState);
//		MyApplication.getInstance().pushActivity(this);
	}

	protected View getTitleBack() {
		return findViewById(R.id.title_back);
	}

	protected TextView getTitleName() {
		return (TextView) findViewById(R.id.title_name);
	}

	protected View getTitleMenu() {
		return findViewById(R.id.title_menu);
	}
	
	protected TextView getTitleMenuName() {
		return (TextView) findViewById(R.id.title_menu_name);
	}
	
	protected void setTitleMenuName(String menuName) {
		TextView menuTextView = (TextView) findViewById(R.id.title_menu_name);
		menuTextView.setText(menuName);
		menuTextView.setVisibility(View.VISIBLE);
	}
	
	protected void setTitleMenuName(int resid) {
		TextView menuTextView = (TextView) findViewById(R.id.title_menu_name);
		menuTextView.setText(resid);
		menuTextView.setVisibility(View.VISIBLE);
	}
	
	protected void setTitleMenuIcon(int resid) {
		ImageView menuImageView = (ImageView) findViewById(R.id.title_menu_icon);
		menuImageView.setImageResource(resid);
		menuImageView.setVisibility(View.VISIBLE);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if(null != this.getCurrentFocus()) {
			CommonUtils.hideSoftKeyboard(this);
		}
		return super .onTouchEvent(event);
	}

	/**
	 * 用于判断是否是重复点击
	 *
	 * @param view
	 * @return
	 */
	protected boolean isRepeatClick(View view) {
		boolean isRepeatClick = false;

		if (view == null) {
			return false;
		}

		if (mLastClickViewId == 0) {
			mLastClickViewId = view.getId();
			mLastClickTime = System.currentTimeMillis();
			return false;
		}

		int tmpClickViewId = view.getId();
		long tmpClickTime = System.currentTimeMillis();

		if (tmpClickViewId == mLastClickViewId) {
			if (tmpClickTime - mLastClickTime <= CLICK_REPEATTIME) {
				isRepeatClick = true;
			}

			mLastClickTime = tmpClickTime;
		} else {
			if (tmpClickTime - mLastClickTime <= CLICK_REPEATTIME) {
				isRepeatClick = true;
			}

			mLastClickTime = tmpClickTime;
			mLastClickViewId = tmpClickViewId;
		}

		return isRepeatClick;
	}
}
