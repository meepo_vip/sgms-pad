package com.fablesoft.projectdatacollect.ui;

import java.util.HashMap;
import java.util.Map;

//import com.fablesoft.pinganbuild.bean.LoginData;
//import com.fablesoft.pinganbuild.bean.LoginResponse;
//import com.fablesoft.pinganbuild.bean.User;








import com.fablesoft.projectdatacollect.MyApplication;
import com.fablesoft.projectdatacollect.R;
import com.fablesoft.projectdatacollect.util.PreferencesUtil;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;

public class SplashActivity extends BaseActivity {

    private final static String TAG = "SplashActivity";
    private final int SPLASH_TIMEOUT = 2000;
    private SharedPreferences mSharedPreferences;
    private int MESSAGE_FINISH_TIMER = 0xf1;
    private int MESSAGE_START_LOGIN = 0xf2;
    private int GO_GUIDED = 0xf3;
    private static long mLastTime;
//    private LoginResponse result;
    private String username; 
	private String password;
	private boolean isAuto = false;

    private Handler mHandler = new Handler() {
        @Override
		public void handleMessage(android.os.Message msg) {
            if (msg.what == MESSAGE_FINISH_TIMER) {
                /**
                 * when we timeout, we has receive the response, so we will
                 * start LoginActivty
                 * */
                mHandler.removeMessages(MESSAGE_FINISH_TIMER);
                mHandler.removeMessages(MESSAGE_START_LOGIN);
                startMainAcitivity();
            } else if (msg.what == MESSAGE_START_LOGIN){
                /**
                 * when we timeout, we has receive the response, so we will
                 * start LoginActivty
                 * */
            	if(isAuto){
//            		if(result == null){
//                		Toast.makeText(SplashActivity.this, R.string.request_error, Toast.LENGTH_SHORT).show();
//                	}else{
//                		Toast.makeText(SplashActivity.this, result.getMessage(), Toast.LENGTH_SHORT).show();
//                	}
            	}
                mHandler.removeMessages(MESSAGE_FINISH_TIMER);
                mHandler.removeMessages(MESSAGE_START_LOGIN);
                startLoginAcitivity();
            } else if (msg.what == GO_GUIDED){
//            	sp.edit().putBoolean("isGuided", true).commit();
//            	Intent intent = new Intent(SplashActivity.this,
//            			AppIntroductActivity.class);
//                SplashActivity.this.startActivity(intent);
//                finish();
            }
        }
    };

    private void startMainAcitivity() {
        Intent intent = new Intent(SplashActivity.this,
        		MainActivity.class);
        SplashActivity.this.startActivity(intent);
        finish();
    }
    
    private void startLoginAcitivity() {
        Intent intent = new Intent(SplashActivity.this,
                LoginActivity.class);
        intent.putExtra("username", username);
        intent.putExtra("password", password);
        SplashActivity.this.startActivity(intent);
        finish();
    }

    @Override
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        mLastTime = System.currentTimeMillis();
        mSharedPreferences = PreferencesUtil.getSharedPreferences(null);
        isAuto = mSharedPreferences.getBoolean("autologin", false);
        /**
         * when password is been saved, we auto login
         * */
		if (mSharedPreferences.getBoolean("isGuided", false)) {
			mHandler.sendEmptyMessageDelayed(GO_GUIDED, SPLASH_TIMEOUT);
		} else if (isAuto) {
            autoLogin();
        } else {
            flashLogin(SPLASH_TIMEOUT);
        }
    }
    
    private void autoLogin() {
    	username = mSharedPreferences.getString("username", "");
    	password = mSharedPreferences.getString("password", "");
    	Map<String, Object> params = new HashMap<String, Object>();
		params.put("loginName", username);
		params.put("password", password);
		MyApplication.getInstance().setSessionId("");
//		Log.i("lzx", Config.LOGIN_URL);
//		HttpThread httpThread = new HttpThread(Config.LOGIN_URL, this, params, LoginResponse.class, 8000);
//    	MyApplication.getInstance().getRequestQueue().execute(httpThread);
    }

    /**
     * if network timeout time > SPLASH_TIMEOUT, we start homepage Immediately.
     * or network timeout time < SPLASH_TIMEOUT, we will delay
     * (SPLASH_TIMEOUT-timeout) time, then start homepage
     * */
    private void startHomeDelay() {
        long currentTime = System.currentTimeMillis();
        long time = currentTime - mLastTime;
        if (SPLASH_TIMEOUT - time <= 0) {
            flashHome(0);
        } else {
            flashHome(SPLASH_TIMEOUT - time);
        }
    }
    
    /**
     * if network timeout time > SPLASH_TIMEOUT, we start homepage Immediately.
     * or network timeout time < SPLASH_TIMEOUT, we will delay
     * (SPLASH_TIMEOUT-timeout) time, then start homepage
     * */
    private void startLoginDelay() {
        long currentTime = System.currentTimeMillis();
        long time = currentTime - mLastTime;
        if (SPLASH_TIMEOUT - time <= 0) {
            flashLogin(0);
        } else {
            flashLogin(SPLASH_TIMEOUT - time);
        }
    }

    /**
     * start the login Activity
     * */
    private void flashLogin(final long time) {
        mHandler.sendEmptyMessageDelayed(MESSAGE_START_LOGIN, time);
    }
    
    /**
     * start the home Activity
     * */
    private void flashHome(final long time) {
        mHandler.sendEmptyMessageDelayed(MESSAGE_FINISH_TIMER, time);
    }
}
