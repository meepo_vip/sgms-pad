package com.fablesoft.projectdatacollect.ui;

import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.fablesoft.projectdatacollect.R;
import com.fablesoft.projectdatacollect.ui.fragment.FragmentAddEdu;
import com.fablesoft.projectdatacollect.ui.fragment.FragmentAddSafe;
import com.fablesoft.projectdatacollect.ui.fragment.FragmentAddTec;

public class SafeCampaignAddActivity extends BaseActivity implements View.OnClickListener {

    private int position;
    private String[] types;
    private TextView tvTitle;
    private Button btCommit;
    private FragmentAddSafe fragmentAddSafe;
    private FragmentAddEdu fragmentAddEdu;
    private FragmentAddTec fragmentAddTec;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_campaign);
        initData();
        initView();
        showFragment();
    }

    private void initView(){
        tvTitle = (TextView) findViewById(R.id.tv_title);
        tvTitle.setText(types[position]);
        btCommit = (Button) findViewById(R.id.bt_commit);
        btCommit.setOnClickListener(this);
        View back = findViewById(R.id.back);
        back.setOnClickListener(this);
    }

    private void initData() {
        position = getIntent().getIntExtra("position", 0);
        types = getResources().getStringArray(R.array.campaigntype);
    }

    private void showFragment() {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        Bundle bundle = new Bundle();
        bundle.putString("type", types[position]);
        switch (position) {
            case 0:
                fragmentAddSafe = new FragmentAddSafe();
                fragmentAddSafe.setArguments(bundle);
                transaction.add(R.id.content, fragmentAddSafe);
                break;
            case 1:
                fragmentAddEdu = new FragmentAddEdu();
                fragmentAddEdu.setArguments(bundle);
                transaction.add(R.id.content, fragmentAddEdu);
                break;
            case 2:
                fragmentAddTec = new FragmentAddTec();
                fragmentAddTec.setArguments(bundle);
                transaction.add(R.id.content, fragmentAddTec);
                break;
        }
        transaction.commit();
    }

    @Override
    public void onClick(View v) {
        if (isRepeatClick(v)){
            return;
        }
        switch (v.getId()){
            case R.id.bt_commit:
                commit();
                break;
            case R.id.back:
                finish();
                break;
        }
    }

    private void commit(){
        switch (position){
            case 0:
                if (fragmentAddSafe != null){
                    fragmentAddSafe.commit();
                }
                break;
            case 1:
                if (fragmentAddEdu != null){
                    fragmentAddEdu.commit();
                }
                break;
            case 2:
                if (fragmentAddTec != null){
                    fragmentAddTec.commit();
                }
                break;
        }
    }
}
