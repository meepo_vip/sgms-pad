package com.fablesoft.projectdatacollect.ui.fragment;

import android.app.Fragment;
import android.content.Context;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.authentication.utils.ToastUtil;
import com.fablesoft.projectdatacollect.interfaces.SwitchListener;
import com.fablesoft.projectdatacollect.ui.PersonnelRegisterNewActivity;
import com.fablesoft.projectdatacollect.util.CommonUtils;

public class BaseFragment extends Fragment implements View.OnClickListener {
    private static final long CLICK_REPEATTIME = 500;
    protected View view;
    protected Context mContext;
    protected PersonnelRegisterNewActivity mActivity;
    protected String imagePath;
    protected SwitchListener listener;
    protected MediaPlayer mediaPlayer;
    protected Button btNext;
    protected Button btLast;
    private long mLastClickTime = 0;
    private int mLastClickViewId = 0;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected void initView() {
        mContext = getActivity();
        mActivity = (PersonnelRegisterNewActivity)getActivity();
    }

    protected void initData() {
        imagePath = Environment.getExternalStorageDirectory() + CommonUtils.TAKE_PHOTO_PATH;
    }

    @Override
    public void onClick(View v) {

    }

    public void setSerialPort(boolean enable) {
    }
    protected void initSerialPort(boolean enable) {
    }

    public void setSwitchListener(SwitchListener listener) {
        this.listener = listener;
    }

    /**
     * 用于判断是否是重复点击
     *
     * @param view
     * @return
     */
    protected boolean isRepeatClick(View view) {
        boolean isRepeatClick = false;

        if (view == null) {
            return false;
        }

        if (mLastClickViewId == 0) {
            mLastClickViewId = view.getId();
            mLastClickTime = System.currentTimeMillis();
            return false;
        }

        int tmpClickViewId = view.getId();
        long tmpClickTime = System.currentTimeMillis();

        if (tmpClickViewId == mLastClickViewId) {
            Log.d("sglei-click", "tmpClickTime = " + tmpClickTime + ", mLastClickTime = " + mLastClickTime + ", time = " + (tmpClickTime - mLastClickTime));
            if (tmpClickTime - mLastClickTime <= CLICK_REPEATTIME) {
                isRepeatClick = true;
            }

            mLastClickTime = tmpClickTime;
        } else {
            if (tmpClickTime - mLastClickTime <= CLICK_REPEATTIME) {
                isRepeatClick = true;
            }

            mLastClickTime = tmpClickTime;
            mLastClickViewId = tmpClickViewId;
        }

        return isRepeatClick;
    }
}
