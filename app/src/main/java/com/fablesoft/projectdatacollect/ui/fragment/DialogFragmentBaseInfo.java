package com.fablesoft.projectdatacollect.ui.fragment;

import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.fablesoft.projectdatacollect.R;
import com.fablesoft.projectdatacollect.bean.DepartmentBean;
import com.fablesoft.projectdatacollect.bean.EducationBean;
import com.fablesoft.projectdatacollect.bean.PersonTypeBean;
import com.fablesoft.projectdatacollect.bean.PositionBean;
import com.fablesoft.projectdatacollect.bean.TeamBean;
import com.fablesoft.projectdatacollect.util.CommonUtils;
import com.fablesoft.projectdatacollect.view.DividerGridItemDecoration;

import java.util.List;

public class DialogFragmentBaseInfo<T> extends DialogFragment implements View.OnClickListener {
    private View view;
    private RecyclerView rvBaseInfo;
    private TextView tvTitle;
    private ImageView ivCancel;

    private List<T> datas;
    private ItemCheckedListener listener;
    private HomeAdapter adapter;
    private int flag;
    private Context context;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        view = inflater.inflate(R.layout.dialog_baseinfo_selector, null);

        tvTitle = (TextView) view.findViewById(R.id.tv_title);
        ivCancel = (ImageView) view.findViewById(R.id.iv_cancel);
        ivCancel.setOnClickListener(this);

        setTitle();

        rvBaseInfo = (RecyclerView) view.findViewById(R.id.rv_baseinfo);
        rvBaseInfo.setLayoutManager(new GridLayoutManager(context, 3));
        rvBaseInfo.setAdapter(adapter = new HomeAdapter());
        rvBaseInfo.addItemDecoration(new DividerGridItemDecoration(context));

        return view;
    }

    private void setTitle() {
        if (flag == CommonUtils.FLAG_PEOPLE_TYPE) {
            tvTitle.setText(String.format(getResources().getString(R.string.title_select), "人员种类"));
        } else if (flag == CommonUtils.FLAG_DEPARTMENT) {
            tvTitle.setText(String.format(getResources().getString(R.string.title_select), "部门"));
        } else if (flag == CommonUtils.FLAG_POSITION) {
            tvTitle.setText(String.format(getResources().getString(R.string.title_select), "岗位"));
        } else if (flag == CommonUtils.FLAG_EDUCATION) {
            tvTitle.setText(String.format(getResources().getString(R.string.title_select), "学历"));
        } else if (flag == CommonUtils.FLAG_TEAM) {
            tvTitle.setText(String.format(getResources().getString(R.string.title_select), "班组"));
        }
    }

    public void setItemCheckedListener(ItemCheckedListener listener) {
        this.listener = listener;
    }

    public void setDatas(List<T> datas) {
        this.datas = datas;
    }

    public void setDatas(List<T> datas, int flag) {
        this.datas = datas;
        this.flag = flag;
    }

    class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.MyViewHolder> {

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            MyViewHolder holder = new MyViewHolder(LayoutInflater.from(getActivity()).inflate(R.layout.item_baseinfo, parent,
                    false));
            return holder;
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, final int position) {
            Object data = datas.get(position);
            if (flag == CommonUtils.FLAG_DEPARTMENT) {
                holder.tv.setText(((DepartmentBean) data).getName());
            } else if (flag == CommonUtils.FLAG_POSITION) {
                holder.tv.setText(((PositionBean) data).getName());
            } else if (flag == CommonUtils.FLAG_EDUCATION) {
                holder.tv.setText(((EducationBean) data).getName());
            } else if (flag == CommonUtils.FLAG_PEOPLE_TYPE) {
                holder.tv.setText(((PersonTypeBean) data).getName());
            } else if (flag == CommonUtils.FLAG_TEAM) {
                holder.tv.setText(((TeamBean) data).getWorkTeamName());
            }
            holder.tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                    listener.onChecked(position, flag);
                }
            });
        }

        @Override
        public int getItemCount() {
            return datas.size();
        }

        class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tv;

            public MyViewHolder(View view) {
                super(view);
                tv = (TextView) view.findViewById(R.id.tv_menu);
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_cancel:
                dismiss();
                break;
        }
    }

    public interface ItemCheckedListener {
        void onChecked(int position, int flag);
    }
}
