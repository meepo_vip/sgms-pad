package com.fablesoft.projectdatacollect.http;


import java.util.Map;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;

public class BaseRequest {
    protected String url;
    protected Map<String, Object> params;
    protected Class clazz;
    protected HttpListener listener;

    public static final String KEY_FILE = "file";

    protected static final String BOUNDARY = "********";
    protected static final String IMGUR_CLIENT_ID = "...";
    protected static final MediaType MEDIA_TYPE_JPG = MediaType.parse("image/jpg");
    protected final OkHttpClient client = new OkHttpClient();

}
