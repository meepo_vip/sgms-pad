package com.fablesoft.projectdatacollect.http;

import com.fablesoft.projectdatacollect.bean.BaseResponse;


/**
 * HTTP请求观察类，实现该接口的类能够观察到HTTP请求发送的情况
 * 
 */
public interface HttpListener
{
    /**
     * HTTP请求处理成功时调用该方法
     * 
     * @param data data
     */
    void onRequestFinish(BaseResponse response);
    
    
    void onNotLogin(BaseResponse response);
}
