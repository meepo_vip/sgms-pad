package com.fablesoft.projectdatacollect.http;

import android.util.Log;

import com.fablesoft.projectdatacollect.MyApplication;
import com.fablesoft.projectdatacollect.bean.BaseResponse;
import com.fablesoft.projectdatacollect.util.CommonUtils;
import com.fablesoft.projectdatacollect.util.Config;
import com.fablesoft.projectdatacollect.util.Log4jUtil;

import org.json.JSONObject;

import java.io.IOException;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class RequestData extends BaseRequest {

    public static final MediaType JSON = MediaType.parse("application/json;charset=utf-8");
    public static final MediaType TEXT = MediaType.parse("text/html;charset=utf-8");

    public RequestData(String url, Map<String, Object> params, Class clazz, HttpListener listener) {
        this.url = url;
        this.params = params;
        this.clazz = clazz;
        this.listener = listener;
    }

    public RequestData(String url, Class clazz, HttpListener listener) {
        this.url = url;
        this.clazz = clazz;
        this.listener = listener;
    }

    public void sendGet() {
        Log.d("sglei-upload", "get url = " + url);
        //构建请求体
        Request.Builder requestBuilder = new Request.Builder();
        requestBuilder.url(url);
        requestBuilder.header("Cookie", MyApplication.getInstance().getSessionId());
        requestBuilder.get();
        Request request = requestBuilder.build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.d(CommonUtils.TAG_UPLOAD, "excetion = " + e.toString());
                listener.onRequestFinish(null);
                Log4jUtil.e(e.toString());
                call.cancel();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response == null || response.body() == null) {
                    listener.onRequestFinish(null);
                    call.cancel();
                    return;
                }
                String result = response.body().string();
                CommonUtils.showLogCompletion(result, 2 * 1024);
                try {
                    BaseResponse baseResponse = JsonUtil.getResponse(result, clazz);
                    if ("403".equals(baseResponse.getCode())) {
                        listener.onNotLogin(baseResponse);
                    } else {
                        listener.onRequestFinish(baseResponse);
                    }
                } catch (Exception e) {
                    Log4jUtil.e(e.toString());
                    e.printStackTrace();
                } finally {
                    Log4jUtil.d(result);
                    call.cancel();
                }
            }
        });
    }

    public void sendFormPost() {
        FormBody.Builder builder = new FormBody.Builder();
        //遍历map中所有参数到builder
        if (params != null) {
            for (String key : params.keySet()) {
                builder.add(key, params.get(key).toString());
            }
        }
        //构建请求体
        RequestBody requestBody = builder.build();
        Request.Builder requestBuilder = new Request.Builder();
        requestBuilder.url(url);
        requestBuilder.header("Cookie", MyApplication.getInstance().getSessionId()).header(Config.CLIENT_TYPE_KEY, Config.CLIENT_TYPE_VALUE);
        requestBuilder.post(requestBody);
        Request request = requestBuilder.build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.d(CommonUtils.TAG_UPLOAD, "excetion = " + e.toString());
                listener.onRequestFinish(null);
                Log4jUtil.e(e.toString());
                call.cancel();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response == null || response.body() == null) {
                    listener.onRequestFinish(null);
                    call.cancel();
                    return;
                }
                String result = response.body().string();
                CommonUtils.showLogCompletion(result, 2 * 1024);
                try {
                    BaseResponse baseResponse = JsonUtil.getResponse(result, clazz);
                    if ("403".equals(baseResponse.getCode())) {
                        listener.onNotLogin(baseResponse);
                    } else {
                        listener.onRequestFinish(baseResponse);
                    }
                } catch (Exception e) {
                    Log4jUtil.e(e.toString());
                    e.printStackTrace();
                } finally {
                    Log4jUtil.d(result);
                    call.cancel();
                }
            }
        });
    }

    public void sendJsonPost() {
        JSONObject json = new JSONObject(params);
        Log.d("sglei-upload", "json.toString() = " + json.toString() + ", url = " + url);
        RequestBody requestBody = RequestBody.create(JSON, json.toString());
        Request request = new Request.Builder().url(url).header("Cookie", MyApplication.getInstance().getSessionId()).post(requestBody).build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.d(CommonUtils.TAG_UPLOAD, "excetion = " + e.toString());
                listener.onRequestFinish(null);
                Log4jUtil.e(e.toString());
                call.cancel();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response == null || response.body() == null) {
                    listener.onRequestFinish(null);
                    call.cancel();
                    return;
                }
                String result = response.body().string();
                CommonUtils.showLogCompletion(result, 2 * 1024);
                try {
                    BaseResponse baseResponse = JsonUtil.getResponse(result, clazz);
                    if ("403".equals(baseResponse.getCode())) {
                        listener.onNotLogin(baseResponse);
                    } else {
                        listener.onRequestFinish(baseResponse);
                    }
                } catch (Exception e) {
                    Log4jUtil.e(e.toString());
                    e.printStackTrace();
                } finally {
                    Log4jUtil.d(result);
                    call.cancel();
                }
            }
        });
    }

    public void getVersionInfo() {
        //构建请求体
        Request.Builder requestBuilder = new Request.Builder();
        requestBuilder.url(url);
        requestBuilder.get();
        Request request = requestBuilder.build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.d(CommonUtils.TAG_UPLOAD, "excetion = " + e.toString());
                listener.onRequestFinish(null);
                Log4jUtil.e(e.toString());
                call.cancel();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response == null || response.body() == null) {
                    listener.onRequestFinish(null);
                    call.cancel();
                    return;
                }
                String result = response.body().string();
                CommonUtils.showLogCompletion(result, 2 * 1024);
                try {
                    BaseResponse baseResponse = JsonUtil.getResponse(result, clazz);
                    if ("403".equals(baseResponse.getCode())) {
                        listener.onNotLogin(baseResponse);
                    } else {
                        listener.onRequestFinish(baseResponse);
                    }
                } catch (Exception e) {
                    Log4jUtil.e(e.toString());
                    e.printStackTrace();
                } finally {
                    Log4jUtil.d(result);
                    call.cancel();
                }
            }
        });
    }
}
