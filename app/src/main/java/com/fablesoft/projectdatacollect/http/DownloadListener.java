package com.fablesoft.projectdatacollect.http;


public interface DownloadListener {
    void onDownloadSuccess(String filepath, String filename);

    void onDownloading(int progress);

    void onDownloadFailed();
}