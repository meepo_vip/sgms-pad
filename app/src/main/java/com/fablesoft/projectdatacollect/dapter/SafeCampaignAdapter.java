package com.fablesoft.projectdatacollect.dapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.fablesoft.projectdatacollect.R;
import com.fablesoft.projectdatacollect.bean.CampaignBean;
import com.fablesoft.projectdatacollect.util.DateUtil;

import java.util.List;

public class SafeCampaignAdapter extends BaseAdapter {
    private Context context;
    private List<CampaignBean> datas;
    private View.OnClickListener listener;
    private int position;

    public SafeCampaignAdapter(Context context, List<CampaignBean> datas, View.OnClickListener listener) {
        this.context = context;
        this.datas = datas;
        this.listener = listener;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(
                    R.layout.campaign_list_item, null);
            viewHolder.name = (TextView) convertView
                    .findViewById(R.id.campaign_name);
            viewHolder.type = (TextView) convertView
                    .findViewById(R.id.campaign_type);
            viewHolder.speaker = (TextView) convertView
                    .findViewById(R.id.campaign_speaker);
            viewHolder.date = (TextView) convertView
                    .findViewById(R.id.campaign_date);
            viewHolder.operateBtn = (TextView) convertView
                    .findViewById(R.id.operate_btn);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if (datas.get(position).getId()
                .equals(viewHolder.name.getTag())) {
            return convertView;
        }
        viewHolder.operateBtn.setTag(position);
        viewHolder.operateBtn.setOnClickListener(listener);
        CampaignBean campaignBean = datas.get(position);
        viewHolder.name.setText(campaignBean.getName());
        viewHolder.type.setText(campaignBean.getTypeName());
        viewHolder.speaker.setText(campaignBean.getSpeaker());
        viewHolder.date.setText(DateUtil.formatStrTime(campaignBean.getStartTime()));
        return convertView;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public int getCount() {
        return datas.size();
    }

    class ViewHolder {
        TextView name;
        TextView type;
        TextView date;
        TextView speaker;
        TextView operateBtn;
    }

}
