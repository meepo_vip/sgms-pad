package com.fablesoft.projectdatacollect.bean;

public class CampaignRegisterResponse extends BaseResponse {

    private CampaignRegisterDataBean data;

    public CampaignRegisterDataBean getData() {
        return data;
    }

    public void setData(CampaignRegisterDataBean data) {
        this.data = data;
    }
}