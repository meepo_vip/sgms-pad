package com.fablesoft.projectdatacollect.bean;

public class PersonCollectBean {
    private IDCardBean idCardBean;

    public IDCardBean getIdCardBean() {
        return idCardBean;
    }

    public void setIdCardBean(IDCardBean idCardBean) {
        this.idCardBean = idCardBean;
    }
}
