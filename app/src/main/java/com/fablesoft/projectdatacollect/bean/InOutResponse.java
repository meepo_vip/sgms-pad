package com.fablesoft.projectdatacollect.bean;

import java.util.List;

public class InOutResponse extends BaseResponse {

    private List<InOutBean> rows;

    public List<InOutBean> getRows() {
        return rows;
    }

    public void setRows(List<InOutBean> rows) {
        this.rows = rows;
    }
}