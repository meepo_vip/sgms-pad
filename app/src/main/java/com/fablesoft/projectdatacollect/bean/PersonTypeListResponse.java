package com.fablesoft.projectdatacollect.bean;

import java.util.List;

public class PersonTypeListResponse extends BaseResponse  {
	
	private List<PersonTypeBean> rows;

	public List<PersonTypeBean> getRows() {
		return rows;
	}

	public void setRows(List<PersonTypeBean> rows) {
		this.rows = rows;
	}
}