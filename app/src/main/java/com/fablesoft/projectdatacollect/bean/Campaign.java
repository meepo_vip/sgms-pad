package com.fablesoft.projectdatacollect.bean;

public class Campaign {
    private String activityId;

    public String getActivityId() {
        return activityId;
    }

    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }
}