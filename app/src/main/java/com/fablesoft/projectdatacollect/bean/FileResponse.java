package com.fablesoft.projectdatacollect.bean;

public class FileResponse extends BaseResponse {
	
	private String data;

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}
}
