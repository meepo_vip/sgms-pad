package com.fablesoft.projectdatacollect.bean;

public class FingerprintBean {
	
	private String type;

	private String imageId;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getImageId() {
		return imageId;
	}

	public void setImageId(String imageId) {
		this.imageId = imageId;
	}

}
