package com.fablesoft.projectdatacollect.bean;

public class InOutBean {
    private String inOutStatus;//1：在场；2：离场‘

    public String getInOutStatus() {
        return inOutStatus;
    }

    public void setInOutStatus(String inOutStatus) {
        this.inOutStatus = inOutStatus;
    }
}

