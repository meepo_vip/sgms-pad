package com.fablesoft.projectdatacollect.bean;

public class FileListResponse extends BaseResponse {
	
	private FileBean[] data;

	public FileBean[] getData() {
		return data;
	}

	public void setData(FileBean[] data) {
		this.data = data;
	}
}
