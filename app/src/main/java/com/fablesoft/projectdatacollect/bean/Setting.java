package com.fablesoft.projectdatacollect.bean;

public class Setting {
    private int id;
    private String serverName;
    private String serverUrl;
    private String projectName;

    public Setting() {
    }

    public Setting(int id, String serverName, String serverUrl, String projectName) {
        this.id = id;
        this.serverName = serverName;
        this.serverUrl = serverUrl;
        this.projectName = projectName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getServerName() {
        return serverName;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    public String getServerUrl() {
        return serverUrl;
    }

    public void setServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }
}
