package com.fablesoft.projectdatacollect.bean;

import java.util.List;

public class AppInfoResponse extends BaseResponse {
    private List<AppInfoBean> rows;

    public List<AppInfoBean> getRows() {
        return rows;
    }

    public void setRows(List<AppInfoBean> rows) {
        this.rows = rows;
    }

    public AppInfoBean getAppInfo(){
        if (rows != null && rows.size() >0){
            return rows.get(0);
        }
        return null;
    }
}
