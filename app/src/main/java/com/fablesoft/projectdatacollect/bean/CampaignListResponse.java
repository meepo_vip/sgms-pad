package com.fablesoft.projectdatacollect.bean;

import java.util.List;

public class CampaignListResponse extends BaseResponse  {
	
	private List<CampaignBean> rows;

	public List<CampaignBean> getRows() {
		return rows;
	}

	public void setRows(List<CampaignBean> rows) {
		this.rows = rows;
	}
}