package com.fablesoft.projectdatacollect.bean;

public class VersionResponse extends BaseResponse {

    private VersionBean data;

    public VersionBean getData() {
        return data;
    }

    public void setData(VersionBean data) {
        this.data = data;
    }
}
