package com.fablesoft.projectdatacollect.bean;

import java.util.List;

public class MemberListResponse extends BaseResponse{
	
	private List<MemberBean> rows;

	public List<MemberBean> getRows() {
		return rows;
	}

	public void setRows(List<MemberBean> rows) {
		this.rows = rows;
	}
}
