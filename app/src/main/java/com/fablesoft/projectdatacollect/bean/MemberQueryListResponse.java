package com.fablesoft.projectdatacollect.bean;

import java.util.List;

public class MemberQueryListResponse extends BaseResponse{
	
	private List<MemberQueryBean> rows;

	public List<MemberQueryBean> getRows() {
		return rows;
	}

	public void setRows(List<MemberQueryBean> rows) {
		this.rows = rows;
	}
}
