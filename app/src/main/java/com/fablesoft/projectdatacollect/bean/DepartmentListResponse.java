package com.fablesoft.projectdatacollect.bean;

import java.util.List;

public class DepartmentListResponse extends BaseResponse  {
	
	private List<DepartmentBean> rows;

	public List<DepartmentBean> getRows() {
		return rows;
	}

	public void setRows(List<DepartmentBean> rows) {
		this.rows = rows;
	}
}