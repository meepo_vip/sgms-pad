package com.fablesoft.projectdatacollect.bean;

import java.util.List;

public class TeamResponse extends BaseResponse {
    private List<TeamBean> rows;

    public List<TeamBean> getRows() {
        return rows;
    }

    public void setRows(List<TeamBean> rows) {
        this.rows = rows;
    }
}
