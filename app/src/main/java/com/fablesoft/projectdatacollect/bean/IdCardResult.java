package com.fablesoft.projectdatacollect.bean;

import java.util.List;

public class IdCardResult {

    private PersonCollectBean personCollectBean;

    private List<ItemFingerprint> fingerPrintPOS;

    private List<FileMaterialBean> orgPersonFileList;

    private BaseInfoNewBean orgPersonBean;

    public List<ItemFingerprint> getFingerPrintPOS() {
        return fingerPrintPOS;
    }

    public void setFingerPrintPOS(List<ItemFingerprint> fingerPrintPOS) {
        this.fingerPrintPOS = fingerPrintPOS;
    }

    public List<FileMaterialBean> getOrgPersonFileList() {
        return orgPersonFileList;
    }

    public void setOrgPersonFileList(List<FileMaterialBean> orgPersonFileList) {
        this.orgPersonFileList = orgPersonFileList;
    }

    public BaseInfoNewBean getOrgPersonBean() {
        return orgPersonBean;
    }

    public void setOrgPersonBean(BaseInfoNewBean orgPersonBean) {
        this.orgPersonBean = orgPersonBean;
    }

    public PersonCollectBean getPersonCollectBean() {
        return personCollectBean;
    }

    public void setPersonCollectBean(PersonCollectBean personCollectBean) {
        this.personCollectBean = personCollectBean;
    }
}
