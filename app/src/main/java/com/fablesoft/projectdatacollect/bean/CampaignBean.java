package com.fablesoft.projectdatacollect.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CampaignBean implements Serializable {
	
	private long createdTime;
	private String id;
	private String type;
	private String name;
	private String speaker;
	private String content;
	private String recorder;
	private String typeName;
	private String organizer;
	private long startTime;
	private String site;
	private ArrayList<FileBean> imageFiles;
	private String sum;
    private String technicalType;
	
	public long getCreatedTime() {
		return createdTime;
	}
	public void setCreatedTime(long createdTime) {
		this.createdTime = createdTime;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSpeaker() {
		return speaker;
	}
	public void setSpeaker(String speaker) {
		this.speaker = speaker;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getRecorder() {
		return recorder;
	}
	public void setRecorder(String recorder) {
		this.recorder = recorder;
	}
	public String getTypeName() {
		return typeName;
	}
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	public String getOrganizer() {
		return organizer;
	}
	public void setOrganizer(String organizer) {
		this.organizer = organizer;
	}
	public long getStartTime() {
		return startTime;
	}
	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}
	public String getSite() {
		return site;
	}
	public void setSite(String site) {
		this.site = site;
	}
	public ArrayList<FileBean> getImageFiles() {
		return imageFiles;
	}
	public void setImageFiles(ArrayList<FileBean> imageFiles) {
		this.imageFiles = imageFiles;
	}
	public String getSum() {
		return sum;
	}
	public void setSum(String sum) {
		this.sum = sum;
	}

    public String getTechnicalType() {
        return technicalType;
    }

    public void setTechnicalType(String technicalType) {
        this.technicalType = technicalType;
    }
}