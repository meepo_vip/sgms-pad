package com.fablesoft.projectdatacollect.bean;

public class CampaignResponse extends BaseResponse {
	
	private CampaignBean data;

	public CampaignBean getData() {
		return data;
	}

	public void setData(CampaignBean data) {
		this.data = data;
	}
}