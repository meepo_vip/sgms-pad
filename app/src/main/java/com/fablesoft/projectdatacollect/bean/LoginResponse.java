package com.fablesoft.projectdatacollect.bean;

public class LoginResponse extends BaseResponse {
	
	private String userName;
	
	private String userId;
	
	private String orgId;

	private OrgBean orgBean;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

    public OrgBean getOrgBean() {
        return orgBean;
    }

    public void setOrgBean(OrgBean orgBean) {
        this.orgBean = orgBean;
    }
}
