package com.fablesoft.projectdatacollect.bean;

import android.graphics.Bitmap;
import android.text.TextUtils;

import com.fablesoft.projectdatacollect.util.Log4jUtil;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;


public class BaseImageRequest {
    protected String id;
    protected String localPath;
    protected String fileName;
    protected String itemName;
    protected Bitmap bitmap;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLocalPath() {
        return localPath;
    }

    public void setLocalPath(String localPath) {
        this.localPath = localPath;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        try {
            if (!TextUtils.isEmpty(fileName)){
                this.fileName = URLDecoder.decode(fileName, "UTF-8");
            }
        } catch (UnsupportedEncodingException e) {
            Log4jUtil.e(e.toString());
            e.printStackTrace();
        }
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }
}
