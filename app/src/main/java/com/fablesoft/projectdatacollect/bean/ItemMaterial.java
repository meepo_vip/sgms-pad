package com.fablesoft.projectdatacollect.bean;


import android.graphics.Bitmap;

public class ItemMaterial extends BaseImageRequest {
    private String localThumbPath;
    private Bitmap thumbBitmap;
    private byte[] imageBytes;
    private int background;
    private String filePath;

    public ItemMaterial() {
    }

    public ItemMaterial(String itemName) {
        this.itemName = itemName;
    }


    public ItemMaterial(String itemName, int background) {
        this.itemName = itemName;
        this.background = background;
    }

    public String getLocalThumbPath() {
        return localThumbPath;
    }

    public void setLocalThumbPath(String localThumbPath) {
        this.localThumbPath = localThumbPath;
    }

    public Bitmap getThumbBitmap() {
        return thumbBitmap;
    }

    public void setThumbBitmap(Bitmap thumbBitmap) {
        this.thumbBitmap = thumbBitmap;
    }

    public byte[] getImageBytes() {
        return imageBytes;
    }

    public void setImageBytes(byte[] imageBytes) {
        this.imageBytes = imageBytes;
    }

    public int getBackground() {
        return background;
    }

    public void setBackground(int background) {
        this.background = background;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
}