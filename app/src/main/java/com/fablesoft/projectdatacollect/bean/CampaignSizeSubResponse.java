package com.fablesoft.projectdatacollect.bean;

public class CampaignSizeSubResponse{

	private String inProgressNum;
	private String toBeFiledNum;

	public String getInProgressNum() {
		return inProgressNum;
	}

	public void setInProgressNum(String inProgressNum) {
		this.inProgressNum = inProgressNum;
	}

	public String getToBeFiledNum() {
		return toBeFiledNum;
	}

	public void setToBeFiledNum(String toBeFiledNum) {
		this.toBeFiledNum = toBeFiledNum;
	}
}
