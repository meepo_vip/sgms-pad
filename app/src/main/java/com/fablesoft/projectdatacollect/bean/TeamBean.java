package com.fablesoft.projectdatacollect.bean;

public class TeamBean {
    private String id; //班组id
    private String workTeamName; //班组名称

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getWorkTeamName() {
        return workTeamName;
    }

    public void setWorkTeamName(String workTeamName) {
        this.workTeamName = workTeamName;
    }
}