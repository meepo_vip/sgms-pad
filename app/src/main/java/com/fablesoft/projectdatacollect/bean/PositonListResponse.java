package com.fablesoft.projectdatacollect.bean;

import java.util.List;

public class PositonListResponse extends BaseResponse  {
	
	private List<PositionBean> rows;

	public List<PositionBean> getRows() {
		return rows;
	}

	public void setRows(List<PositionBean> rows) {
		this.rows = rows;
	}
}