package com.fablesoft.projectdatacollect.bean;


public class FileMaterialBean {

    private String id;

    private ItemMaterial fileBean;

    public ItemMaterial getFileBean() {
        fileBean.setId(id);
        return fileBean;
    }

    public void setFileBean(ItemMaterial fileBean) {
        this.fileBean = fileBean;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
