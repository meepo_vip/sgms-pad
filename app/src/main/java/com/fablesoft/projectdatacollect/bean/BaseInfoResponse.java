package com.fablesoft.projectdatacollect.bean;

public class BaseInfoResponse extends BaseResponse {

    private BaseInfoBean data;

    public BaseInfoBean getData() {
        return data;
    }

    public void setData(BaseInfoBean data) {
        this.data = data;
    }
}