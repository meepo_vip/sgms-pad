package com.fablesoft.projectdatacollect.bean;

import java.util.List;

public class EducationListResponse extends BaseResponse  {
	
	private List<EducationBean> rows;

	public List<EducationBean> getRows() {
		return rows;
	}

	public void setRows(List<EducationBean> rows) {
		this.rows = rows;
	}
}