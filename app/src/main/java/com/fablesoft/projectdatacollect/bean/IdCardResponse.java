package com.fablesoft.projectdatacollect.bean;

public class IdCardResponse extends BaseResponse {
	
	private CampaignSizeSubResponse data;

	public CampaignSizeSubResponse getData() {
		return data;
	}

	public void setData(CampaignSizeSubResponse data) {
		this.data = data;
	}
}
