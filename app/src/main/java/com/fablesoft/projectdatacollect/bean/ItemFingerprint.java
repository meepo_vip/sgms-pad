package com.fablesoft.projectdatacollect.bean;


import android.graphics.Bitmap;

import java.io.File;

public class ItemFingerprint extends BaseImageRequest {
    private byte[] imageBytes;
    private String fingerImagePath;

    public ItemFingerprint() {
    }

    public ItemFingerprint(String itemName) {
        this.itemName = itemName;
    }

    public byte[] getImageBytes() {
        return imageBytes;
    }

    public void setImageBytes(byte[] imageBytes) {
        this.imageBytes = imageBytes;
    }

    public String getFingerImagePath() {
        return fingerImagePath;
    }

    public void setFingerImagePath(String fingerImagePath) {
        this.fingerImagePath = fingerImagePath;
    }
}
