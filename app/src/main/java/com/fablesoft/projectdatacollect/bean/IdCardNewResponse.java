package com.fablesoft.projectdatacollect.bean;

public class IdCardNewResponse extends BaseResponse {

    private IdCardResult data;

    public IdCardResult getData() {
        return data;
    }

    public void setData(IdCardResult data) {
        this.data = data;
    }
}
