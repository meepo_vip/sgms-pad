package com.fablesoft.projectdatacollect.bean;

public class DepartmentBean {
	
	private String id;//部门id",
	private String orgId;//机构id",
	private String name;//部门名称",
	private String code;//部门编码",
	private String typeCode;//部门类型，01代表项目部，02代表施工队"
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getOrgId() {
		return orgId;
	}
	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getTypeCode() {
		return typeCode;
	}
	public void setTypeCode(String typeCode) {
		this.typeCode = typeCode;
	}
}