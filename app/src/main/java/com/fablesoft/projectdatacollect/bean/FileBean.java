package com.fablesoft.projectdatacollect.bean;

import java.io.Serializable;

public class FileBean implements Serializable{
	
	private String id;
    private String filepath;
    private String fileserverpath;
    private String fileName;
    private String recordNo;
    private String module;
    private String filePath;
    
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getRecordNo() {
		return recordNo;
	}

	public void setRecordNo(String recordNo) {
		this.recordNo = recordNo;
	}

	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public String getFilePath() {
		return filePath == null ? null : filePath.replaceAll("\\\\", "/");
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getFilepath() {
		return filepath;
	}

	public void setFilepath(String filepath) {
		this.filepath = filepath;
	}

	public String getFileserverpath() {
		return fileserverpath;
	}

	public void setFileserverpath(String fileserverpath) {
		this.fileserverpath = fileserverpath;
	}
}
