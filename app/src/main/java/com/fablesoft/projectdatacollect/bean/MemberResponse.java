package com.fablesoft.projectdatacollect.bean;

import java.util.List;


public class MemberResponse extends BaseResponse{
	
	private List<MemberBean> rows;
	
	private MemberBean data;

	public MemberBean getData() {
		return data;
	}

	public void setData(MemberBean data) {
		this.data = data;
	}

	public List<MemberBean> getRows() {
		return rows;
	}

	public void setRows(List<MemberBean> rows) {
		this.rows = rows;
	}
}
