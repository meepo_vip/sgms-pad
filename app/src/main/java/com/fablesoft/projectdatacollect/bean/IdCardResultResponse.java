package com.fablesoft.projectdatacollect.bean;

public class IdCardResultResponse extends BaseResponse {

    private String photoId;
    private String customizePhotoId;

    private IdCardResult data;

    public String getCustomizePhotoId() {
        return customizePhotoId;
    }

    public void setCustomizePhotoId(String customizePhotoId) {
        this.customizePhotoId = customizePhotoId;
    }

    public String getPhotoId() {

        return photoId;
    }

    public void setPhotoId(String photoId) {
        this.photoId = photoId;
    }

    public IdCardResult getData() {
        return data;
    }

    public void setData(IdCardResult data) {
        this.data = data;
    }
}
