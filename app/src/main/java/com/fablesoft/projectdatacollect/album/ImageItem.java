package com.fablesoft.projectdatacollect.album;

import java.io.Serializable;

import android.graphics.Bitmap;


public class ImageItem implements Serializable {
	public String imageId;
	public String thumbnailPath;
	public String imagePath;
	private Bitmap bitmap;
	private boolean takePhoto;
	public boolean isSelected = false;
	
	public String getImageId() {
		return imageId;
	}
	public void setImageId(String imageId) {
		this.imageId = imageId;
	}
	public String getThumbnailPath() {
		return thumbnailPath;
	}
	public void setThumbnailPath(String thumbnailPath) {
		this.thumbnailPath = thumbnailPath;
	}
	public String getImagePath() {
		return imagePath;
	}
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
	public boolean isSelected() {
		return isSelected;
	}
	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}
	public boolean isTakePhoto() {
		return takePhoto;
	}
	public void setTakePhoto(boolean takePhoto) {
		this.takePhoto = takePhoto;
	}
	
//	public Bitmap getBitmap() {		
//		if(bitmap == null){
//			bitmap = BitmapUtil.decodeThumbBitmapForFile(imagePath, width, height);
//		}
//		return BitmapCache.getInstance().get(imagePath);
//	}
//	public void setBitmap(Bitmap bitmap) {
//		this.bitmap = bitmap;
//	}
	
	
	
}
