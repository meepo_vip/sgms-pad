package com.fablesoft.projectdatacollect.album;

import java.util.List;

import com.fablesoft.projectdatacollect.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * 这个是显示所有包含图片的文件夹的适配器
 *
 * @author king
 * @QQ:595163260
 * @version 2014年10月18日  下午11:49:44
 */
public class FolderAdapter extends BaseAdapter {

	private Context mContext;
	private DisplayMetrics dm;
	private List<ImageBucket> contentList;
	private int selectedPosition;
//	BitmapCache cache;
	private int imageViewWidth;
	final String TAG = getClass().getSimpleName();
	private DisplayImageOptions mOptions;

	public FolderAdapter(Context c, List<ImageBucket> contentList) {
//		this.cache = cache;
		if(mOptions == null){
			mOptions = new DisplayImageOptions.Builder()
			.imageScaleType(ImageScaleType.IN_SAMPLE_INT)
			.resetViewBeforeLoading(true).cacheOnDisc(false)
			.bitmapConfig(Bitmap.Config.RGB_565)
			.considerExifParams(true).build();
		}
		this.contentList = contentList;
		init(c);
	}
	
	// 初始化
	public void init(Context c) {
		mContext = c;
		dm = new DisplayMetrics();
		((Activity) mContext).getWindowManager().getDefaultDisplay()
				.getMetrics(dm);
		imageViewWidth = dm.widthPixels / 4;
	}

	public void setSelectedFile(int position){
		selectedPosition = position;
	}

	@Override
	public int getCount() {
		return contentList.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}
	
//	ImageCallback callback = new ImageCallback() {
//		@Override
//		public void imageLoad(ImageView imageView, Bitmap bitmap,
//				Object... params) {
//			if (imageView != null && bitmap != null) {
//				String url = (String) params[0];
//				if (url != null && url.equals((String) imageView.getTag())) {
//					((ImageView) imageView).setImageBitmap(bitmap);
//				} else {
//					Log.e(TAG, "callback, bmp not match");
//				}
//			} else {
//				Log.e(TAG, "callback, bmp null");
//			}
//		}
//	};

	private class ViewHolder {
		//
		public ImageView backImage;
		// 封面
		public ImageView imageView;
		// 文件夹名称
		public TextView folderName;
		// 文件夹里面的图片数量
		public TextView fileNum;
		// 已选标志
		private ImageView selectedFile;
	}
	ViewHolder holder = null;
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(
					R.layout.plugin_camera_select_folder, null);
			holder = new ViewHolder();
			holder.backImage = (ImageView) convertView
					.findViewById(R.id.file_back);
			holder.imageView = (ImageView) convertView
					.findViewById(R.id.file_image);
			holder.folderName = (TextView) convertView.findViewById(R.id.name);
			holder.fileNum = (TextView) convertView.findViewById(R.id.filenum);
			holder.selectedFile = (ImageView)convertView.findViewById(R.id.selected_file);
			holder.imageView.setAdjustViewBounds(true);
//			LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,dipToPx(65)); 
//			lp.setMargins(50, 0, 50,0); 
//			holder.imageView.setLayoutParams(lp);
			holder.imageView.setScaleType(ImageView.ScaleType.FIT_XY);
			convertView.setTag(holder);
		} else{
			holder = (ViewHolder) convertView.getTag();
		}
		String path = "";
		if (contentList.size() > 0 && contentList.get(position).imageList != null && contentList.get(position).imageList.size() > 0) {
			
			//path = photoAbsolutePathList.get(position);
			//封面图片路径
			if(contentList.get(position).imageList.get(0).isTakePhoto()){
				path = contentList.get(position).imageList.get(1).imagePath;
			}else{
				path = contentList.get(position).imageList.get(0).imagePath;
			}
			
			// 给folderName设置值为文件夹名称
			//holder.folderName.setText(fileNameList.get(position));
			holder.folderName.setText(contentList.get(position).bucketName);
			
			// 给fileNum设置文件夹内图片数量
			//holder.fileNum.setText("" + fileNum.get(position));
			holder.fileNum.setText(contentList.get(position).count + "张");
			
		}
		if (!TextUtils.isEmpty(path)){
//			holder.imageView.setImageResource(R.drawable.plugin_camera_no_pictures);
//		else {
//			holder.imageView.setImageBitmap( AlbumActivity.contentList.get(position).imageList.get(0).getBitmap());
//			final ImageItem item = contentList.get(position).imageList.get(0);
			holder.imageView.setTag(path);
//			cache.displayBmp(holder.imageView, item.imagePath, imageViewWidth,
//					true, callback);
			ImageLoader.getInstance().displayImage("file://" + path, holder.imageView, mOptions);
			if(selectedPosition == position){
				holder.selectedFile.setVisibility(View.VISIBLE);
			}else{
				holder.selectedFile.setVisibility(View.GONE);
			}
		}
		
		return convertView;
	}

	public int dipToPx(int dip) {
		return (int) (dip * dm.density + 0.5f);
	}

}
