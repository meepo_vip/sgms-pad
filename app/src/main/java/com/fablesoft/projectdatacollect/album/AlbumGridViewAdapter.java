package com.fablesoft.projectdatacollect.album;

import java.io.File;
import java.util.ArrayList;

import com.fablesoft.projectdatacollect.R;
import com.fablesoft.projectdatacollect.util.StoragePathsManager;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;

/**
 * 这个是显示一个文件夹里面的所有图片时用的适配器
 *
 * @author king
 * @QQ:595163260
 * @version 2014年10月18日  下午11:49:35
 */
public class AlbumGridViewAdapter extends BaseAdapter{
	final String TAG = getClass().getSimpleName();
	private Context mContext;
	private ArrayList<ImageItem> dataList;
	private DisplayMetrics dm;
//	BitmapCache cache;
	private int imageViewWidth;
	private DisplayImageOptions mOptions;
	
	public AlbumGridViewAdapter(Context c) {
		mContext = c;
//		cache = BitmapCache.getInstance();
		if(mOptions == null){
			mOptions = new DisplayImageOptions.Builder()
			.imageScaleType(ImageScaleType.IN_SAMPLE_INT)
//			.showImageOnLoading(R.drawable.plugin_camera_no_pictures)
			.resetViewBeforeLoading(true).cacheOnDisc(false)
			.cacheInMemory(true)
			.bitmapConfig(Bitmap.Config.RGB_565)
			.considerExifParams(true).build();
		}
		dm = new DisplayMetrics();
		((Activity) mContext).getWindowManager().getDefaultDisplay()
				.getMetrics(dm);
		imageViewWidth = dm.widthPixels / 4;
	}
	
	public void setDataList(ArrayList<ImageItem> dataList){
		this.dataList = dataList;
	}
	
	@Override
	public int getCount() {
		return dataList.size();
	}

	@Override
	public Object getItem(int position) {
		return dataList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

//	ImageCallback callback = new ImageCallback() {
//		@Override
//		public void imageLoad(ImageView imageView, Bitmap bitmap,
//				Object... params) {
//			if (imageView != null && bitmap != null) {
//				String url = (String) params[0];
//				if (url != null && url.equals((String) imageView.getTag())) {
//					((ImageView) imageView).setImageBitmap(bitmap);
//				} else {
//					Log.e(TAG, "callback, bmp not match");
//				}
//			} else {
//				Log.e(TAG, "callback, bmp null");
//			}
//		}
//	};
	
	/**
	 * 存放列表项控件句柄
	 */
	private class ViewHolder {
		public ImageView imageView;
		public RelativeLayout imageViewSelected;
		public View imageLayout;
		public View takePhoto;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder;
		if (convertView == null) {
			viewHolder = new ViewHolder();
			convertView = LayoutInflater.from(mContext).inflate(
					R.layout.plugin_camera_select_imageview, parent, false);
			viewHolder.imageView = (ImageView) convertView
					.findViewById(R.id.image_view);
			RelativeLayout.LayoutParams pa = new RelativeLayout.LayoutParams(imageViewWidth, (int)(imageViewWidth * 0.8));
			viewHolder.imageView.setLayoutParams(pa);
			viewHolder.imageViewSelected = (RelativeLayout) convertView
					.findViewById(R.id.image_view_selected);
			viewHolder.imageViewSelected.setLayoutParams(pa);
			viewHolder.takePhoto = convertView.findViewById(R.id.take_photo);
			viewHolder.imageLayout = convertView.findViewById(R.id.image_layout);
			viewHolder.takePhoto.setLayoutParams(pa);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		final ImageItem item = dataList.get(position);
		if(item.isTakePhoto()){
			viewHolder.takePhoto.setVisibility(View.VISIBLE);
			viewHolder.imageLayout.setVisibility(View.GONE);
			viewHolder.takePhoto.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					StoragePathsManager mStoragePathsManager = new StoragePathsManager(
							mContext);
					String baseDirString = mStoragePathsManager.getInternalStoragePath() + "/pingan/";
					File dir = new File(baseDirString);
					if(!dir.exists()){
						dir.mkdirs();
					}
					String takePhotoPath = baseDirString + System.currentTimeMillis() + ".jpg";
					((ShowAllPhotoActivity) mContext).takePhotoUrl = takePhotoPath;
					Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);// 调用android自带的照相机
					intent.putExtra(MediaStore.EXTRA_OUTPUT,
							Uri.fromFile(new File(takePhotoPath)));
					intent.putExtra("return-data", false);
					((Activity) mContext).startActivityForResult(intent, 0);
				}
			});
		}else{
			viewHolder.takePhoto.setVisibility(View.GONE);
			viewHolder.imageLayout.setVisibility(View.VISIBLE);
			viewHolder.imageView.setTag(item.imagePath);
			ImageLoader.getInstance().displayImage("file://" + item.imagePath, viewHolder.imageView, mOptions);
			boolean isContains = false;
			for(ImageItem imageItem : ShowAllPhotoActivity.tempSelectBitmap){
				if(imageItem.getImagePath().equals(dataList.get(position).getImagePath())){
					isContains = true;
					break;
				}
			}
			if (isContains) {
				viewHolder.imageViewSelected.setVisibility(View.VISIBLE);
			} else {
				viewHolder.imageViewSelected.setVisibility(View.GONE);
			}
		}
		return convertView;
	}
}
