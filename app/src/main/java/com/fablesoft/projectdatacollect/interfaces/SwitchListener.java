package com.fablesoft.projectdatacollect.interfaces;

public interface SwitchListener {
    void onNext(int position);
    void onLast(int position);
    void onSubmitBaseInfo();
    void onSubmitOthers();
    void onSubmitStatus(int flag, boolean isSuccess, boolean isCancel);
}
