package com.fablesoft.projectdatacollect.util;

public class Config {

//    public static String DOMAON = "sgms";
//    public static String BASE = "http://smart.zhinengjianshe.com/";
//	public static String BASE = "http://njwq.zhinengjianshe.com/"; // 南京五桥
//    public static String BASE = "http://sgms.zhinengjianshe.com/"; // 海启
//	public static String BASE = "http://xtsgms.zhinengjianshe.com/"; // 锡通
//    public static String BASE = "http://bh328.zhinengjianshe.com/";//滨海
//    public static String BASE = "http://wfs.zhinengjianshe.com/";//五峰山
//    public static String BASE = "http://nbtd.zhinengjianshe.com/";//南部通道
//  public static String BASE = "http://192.168.1.23:8080/";
//    public static String BASE = "http://192.168.1.26:8080/";
//      public static String BASE = "http://192.168.1.199:9090/";
//    public static String BASE = "http://192.168.1.200:8080/";
//    public static String BASE = "http://192.168.1.200:8081/";
//    public static String BASE = "http://192.168.1.22:8080/";

    public static String BASE;
    public static String BASE_WITH_DOMAIN;
    public static final String CLIENT_TYPE_KEY = "clientType";
    public static final String CLIENT_TYPE_VALUE = "5";
    //	public static String BASE_URL = BASE + DOMAON;
//    public static String BASE_URL;

    public static String LOGIN_URL;//登录
    public static String CONSTRUCTION_TEAMS_URL;//部门
    public static String REGISTER_URL;//登记
    public static String CAMPAIGN_LIST_URL;//安全活动列表
    public static String CAMPAIGN_DETAIL_URL;//安全活动详情
    public static String SUBMIT_CAMPAIGN_URL;//提交安全活动
    public static String SIGN_LIST_URL;//签到列表
    public static String SIGN_URL;//指纹签到
    public static String UPLOAD_FILE_URL;//上传图片
    public static String UPLOAD_FILES_URL;//上传图片列表
    public static String SIGN_IN_UPLOAD_FILES_URL;//上传安全活动图片列表
    public static String DELETE_SIGN_IN_URL;//签退
    public static String DELETE_FILE_URL;//删除文件
    public static String GET_BASEINFO_URL; //获取基本信息列表
    public static String GET_POSITION_URL; //查询岗位列表

    //安全活动
    public static String CAMPAIGN_LIST_NEW_URL; //安全活动列表V2
    public static String CAMPAIGN_LIST_SIZE_URL; //查询进行中和待归档数量
    public static String CAMPAIGN_QUERY_REGISTER_URL; //查询已注册人员
    public static String CAMPAIGN_UNRECOGNIZED_URL; //无法识别,登记

    //人员登记
    public static String UPLOAD_IDCARD_URL; //上传指纹
    public static String UPLOAD_FINGERS_URL; //上传身份证
    public static String UPLOAD_BASEINFO_URL; //上传基本信息
    public static String UPLOAD_MATERIAL_URL; //上传材料照片
    public static String UPLOAD_SMARTCARD_URL; //上传智能卡
    public static String UPLOAD_CHECK_URL;//获取进出场状态

    public static String APP_CHECKUPDATE_URL; //检查更新

    public static String APP_INFO_URL; //获取当前app信息

    public static String GET_TEAM_URL; //获取班组列表

    public static String ADD_CAMPAGIN_SAFE;//新增安全会议
    public static String ADD_CAMPAGIN_EDU;//新增教育培训
    public static String ADD_CAMPAGIN_TEC;//新增技术交底

    public static void initUrl(String url) {
        String[] urls = url.split("/");
        if (urls == null || urls.length <= 2) {
            return;
        }
        BASE = "http://" + urls[2] + "/";
        BASE_WITH_DOMAIN = url;
        LOGIN_URL = BASE_WITH_DOMAIN + "/api/v1/login";//登录
        CONSTRUCTION_TEAMS_URL = BASE_WITH_DOMAIN + "/api/v1/codeDict/dept/query";//部门
        REGISTER_URL = BASE_WITH_DOMAIN + "/api/v1/identity/addIDCard";//登记
        CAMPAIGN_LIST_URL = BASE_WITH_DOMAIN + "/api/v1/safeActivity/query";//安全活动列表
        CAMPAIGN_DETAIL_URL = BASE_WITH_DOMAIN + "/api/v1/safeActivity/training/get";//安全活动详情
        SUBMIT_CAMPAIGN_URL = BASE_WITH_DOMAIN + "/api/v1/safeActivity/submit?id=";//提交安全活动
        SIGN_LIST_URL = BASE_WITH_DOMAIN + "/api/v1/safeActivity/sign/query";//签到列表
        SIGN_URL = BASE_WITH_DOMAIN + "/api/v1/safeActivity/sign/fingerSignIn?safeActivityId=";//指纹签到
        UPLOAD_FILE_URL = BASE_WITH_DOMAIN + "/api/v1/common/uploadFile";//上传图片
        UPLOAD_FILES_URL = BASE_WITH_DOMAIN + "/api/v1/common/uploadListFile";//上传图片列表
        SIGN_IN_UPLOAD_FILES_URL = BASE_WITH_DOMAIN + "/api/v1/safeActivity/uploadListFile";//上传安全活动图片列表
        DELETE_SIGN_IN_URL = BASE_WITH_DOMAIN + "/api/v1/safeActivity/sign/out?id=";//签退
        DELETE_FILE_URL = BASE_WITH_DOMAIN + "/api/v1/safeActivity/deleteImage";//删除文件
        GET_BASEINFO_URL = BASE_WITH_DOMAIN + "/api/v1/codeDict/systemSettingCodeDictList?typeName="; //获取基本信息列表
        GET_POSITION_URL = BASE_WITH_DOMAIN + "/api/v1/codeDict/postion/query"; //查询岗位列表
        CAMPAIGN_LIST_NEW_URL = BASE_WITH_DOMAIN + "/api/v1/safeActivity/queryActivityList"; //安全活动列表V2
        CAMPAIGN_LIST_SIZE_URL = BASE_WITH_DOMAIN + "/api/v1/safeActivity/queryStatusNum"; //查询进行中和待归档数量
        CAMPAIGN_QUERY_REGISTER_URL = BASE_WITH_DOMAIN + "/api/v1/safeActivity/queryRegisterList"; //查询已注册人员
        CAMPAIGN_UNRECOGNIZED_URL = BASE_WITH_DOMAIN + "/api/v1/safeActivity/unrecognized/register"; //无法识别,登记
        UPLOAD_IDCARD_URL = BASE_WITH_DOMAIN + "/api/vi/personBasicInfo/sfzCard/add"; //上传指纹
        UPLOAD_FINGERS_URL = BASE_WITH_DOMAIN + "/api/vi/personBasicInfo/fingerPrint/add"; //上传身份证
        UPLOAD_BASEINFO_URL = BASE_WITH_DOMAIN + "/api/vi/personBasicInfo/addBasicInfo"; //上传基本信息
        UPLOAD_MATERIAL_URL = BASE_WITH_DOMAIN + "/api/vi/personBasicInfo/addFile"; //上传材料照片
        UPLOAD_SMARTCARD_URL = BASE_WITH_DOMAIN + "/api/vi/personBasicInfo/addRfidCard"; //上传智能卡
        UPLOAD_CHECK_URL = BASE_WITH_DOMAIN + "/api/v1/staffInOut/query";//获取进出场状态
        APP_CHECKUPDATE_URL = BASE_WITH_DOMAIN + "/sys/api/version/getNewestVersion"; //检查更新
        APP_INFO_URL = BASE_WITH_DOMAIN + "/sys/api/paramConfig/query";
        GET_TEAM_URL = BASE_WITH_DOMAIN + "/api/v1/workTeam/query";
        ADD_CAMPAGIN_SAFE = BASE_WITH_DOMAIN + "/api/v1/safeActivity/meeting/add";
        ADD_CAMPAGIN_EDU = BASE_WITH_DOMAIN + "/api/v1/safeActivity/training/add";
        ADD_CAMPAGIN_TEC = BASE_WITH_DOMAIN + "/api/v1/safeActivity/disclosure/add";
    }
}

