package com.fablesoft.projectdatacollect.util;

import android.util.Log;

import com.fablesoft.projectdatacollect.MyApplication;


public class CheckUpdateVersion {

    private final static String TAG = "sglei-update";
    private final static int VERSION_NUMBER_LENTH = 3;

    public static boolean isUpdate(final String version) {
        if (version == null) {
            return false;
        }
        String serverVersion = version;
        String localVersion = MyApplication.getLocalVersion();

        Log.d("sglei-update", "serverVersion = " + serverVersion + ", localVersion = " + localVersion);

        String local[] = localVersion.split("\\.");
        String server[] = serverVersion.split("\\.");

        for (int i = 0; i < VERSION_NUMBER_LENTH; i++) {
            if (local.length <= i || local.length <= i) {
                return false;
            }
            if (local[i].compareTo(server[i]) < 0) {
                return true;
            }
        }
        return false;
    }
}
