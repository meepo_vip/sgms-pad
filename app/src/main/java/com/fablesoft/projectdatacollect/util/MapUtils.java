package com.fablesoft.projectdatacollect.util;

import java.text.DecimalFormat;
import android.content.*;

public class MapUtils {

	//dip转换px
	public static int dip2px(Context context, float dipValue){ 
        final float scale = context.getResources().getDisplayMetrics().density; 
        return (int)(dipValue * scale + 0.5f); 
    } 
	//px转换dip
	public static int px2dip(Context context, float pxValue){ 
        final float scale = context.getResources().getDisplayMetrics().density; 
        return (int)(pxValue / scale + 0.5f); 
    } 
	// 距离转换
	public static String distanceFormatter(int distance) {
		if (distance < 1000) {
			return distance + "米";
		} else if (distance % 1000 == 0) {
			return distance / 1000 + "公里";
		} else {
			DecimalFormat df = new DecimalFormat("0.0");
			int a1 = distance / 1000; // 十位

			double a2 = distance % 1000;
			double a3 = a2 / 1000; // 得到个位

			String result = df.format(a3);
			double total = Double.parseDouble(result) + a1;
			return total + "公里";
		}
	}

	//距离转换en 
	public static String distanceFormatterEn(int distance) {
		if (distance < 1000) {
			return distance + "m";
		} else if (distance % 1000 == 0) {
			return distance / 1000 + "km";
		} else {
			DecimalFormat df = new DecimalFormat("0.0");
			int a1 = distance / 1000; // 十位

			double a2 = distance % 1000;
			double a3 = a2 / 1000; // 得到个位

			String result = df.format(a3);
			double total = Double.parseDouble(result) + a1;
			return total + "km";
		}
	}
	// 时间转换
	public static String timeFormatter(int second) {
		int minute = second / 60;
		if (second <= 60) {
			return "1 分钟";
		} else if (minute < 60 && minute > 1) {

			return minute + "分钟";
			// return hour + "小时" + minute1 + "分钟";
		} else {
			int hour = minute / 60;
			int minute2 = minute % 60;
			return hour + "小时 " + minute2 + "分钟";

		}

	}

}
