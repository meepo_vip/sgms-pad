package com.fablesoft.projectdatacollect.util;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;
import android.widget.RadioGroup;

import com.fablesoft.projectdatacollect.MyApplication;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommonUtils {
    public final static int SFZ_CODE = 102;
    public final static int M1_CODE = 103;
    public final static int GET_CONSTRUCTION_TEAMS_CODE = 105;
    public final static int GET_POSITION_CODE = 106;
    public final static int GET_PERSONTYPE_CODE = 107;
    public final static int GET_EDUCATION_CODE = 108;
    public final static int GET_TEAM_CODE = 109;

    public final static int UPLOAD_ID_CARD_IMAGE = 301;
    public final static int UPLOAD_FINGERPRINT_IMAGE = 303;
    public final static int UPLOAD_CERTIFICATER_IMAGE = 304;
    public final static int UPLOAD_BASEINFO = 305;
    public final static int UPLOAD_SMART_CARD = 306;
    public final static int UPLOAD_CHECK = 307;

    public final static String DOT = ".";
    public final static String LINE = "-";

    public static float TAG_ELEVATION = 32;
    public static final int FLAG_PEOPLE_TYPE = 0x0001;
    public static final int FLAG_DEPARTMENT = 0x0002;
    public static final int FLAG_POSITION = 0x0003;
    public static final int FLAG_EDUCATION = 0x0004;
    public static final int FLAG_TEAM = 0x0005;

    public static final int IMAGE_OPEN = 0X1000;
    public static final int IMAGE_TAKE = 0X1001;
    public static final int IMAGE_PICK = 0X1002;

    public static final String REGULAR_PHONE = "^1[34578][0-9]{9}$";

    public static final String TAKE_PHOTO_PATH = "/Android/data/file/images/";
    public static final String APK_SAVE_PATH = "/Android/data/file/apks/";
    public static final String DB_PATH = "/Android/data/file/dbs/";

    public static final String STATUS_IN_PROCESS = "1";
    public static final String STATUS_SUBMITED = "2";

    public static final String TAG_UPLOAD = "sglei-upload";

    public static final String SIGN_BROADCAST_RECEIVER = "com.fablesoft.projectdatacollect.intent.mybroadcastreceiver";

    public static final int SETTING_URL_ID = 1;

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (inputMethodManager.isActive()) {
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        }
    }

    public static void disableRadioGroup(RadioGroup radioGroup) {
        for (int i = 0; i < radioGroup.getChildCount(); i++) {
            radioGroup.getChildAt(i).setEnabled(false);
        }
    }

    public static void enableRadioGroup(RadioGroup radioGroup) {
        for (int i = 0; i < radioGroup.getChildCount(); i++) {
            radioGroup.getChildAt(i).setEnabled(true);
        }
    }

    public static String listToString(List<String> list) {
        if (list == null || list.size() <= 0) {
            return " ";
        }
        String str = list.toString();
        Log.d("sglei-str", "str = " + str);
        str = str.trim();
        str = str.substring(1, str.length() - 1);
        Log.d("sglei-str", "sub = " + str);
        return str;
    }

    public static void showLogCompletion(String log,int showCount){
        if(log.length() >showCount){
            String show = log.substring(0, showCount);
            Log.d(CommonUtils.TAG_UPLOAD, show+"");
            if((log.length() - showCount)>showCount){//剩下的文本还是大于规定长度
                String partLog = log.substring(showCount,log.length());
                showLogCompletion(partLog, showCount);
            }else{
                String surplusLog = log.substring(showCount, log.length());
//              System.pop_out.println(surplusLog);
                Log.d(CommonUtils.TAG_UPLOAD, surplusLog+"");
            }

        }else{
//          System.pop_out.println(log);
            Log.d(CommonUtils.TAG_UPLOAD, log+"");
        }
    }

    /**
     * 校验手机号
     * @param phone
     * @return
     */
    public static boolean checkIsPhone(String phone) {
        Pattern p = Pattern.compile(REGULAR_PHONE);
        Matcher m = p.matcher(phone);
        return m.matches();
    }
}
