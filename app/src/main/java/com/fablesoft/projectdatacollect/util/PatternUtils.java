package com.fablesoft.projectdatacollect.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.text.TextUtils;

public class PatternUtils {

    public static boolean isZhongYing(String str) {

        if (str == null) {
            return false;
        }
        String zhongyin = "^[A-Za-z\u4e00-\u9fa5]+$";// 中英文

        Pattern p = Pattern.compile(zhongyin);

        return p.matcher(str).matches();

    }

    public static boolean isShuZiYing(String str) {

        if (str == null) {
            return false;
        }
        String yinshuzi = "^[A-Za-z0-9]+$";// 字母数字_ @

        Pattern p = Pattern.compile(yinshuzi);

        return p.matcher(str).matches();

    }

    public static boolean isNumber(String str) {

        if (str == null) {
            return false;
        }
        String yinshuzi = "^[0-9]+$";// 字母数字_ @

        Pattern p = Pattern.compile(yinshuzi);

        return p.matcher(str).matches();

    }
    public static boolean isNumberCharChinese(String str) {

        if (str == null) {
            return false;
        }
        String yinshuzi = "^[A-Za-z0-9\u4e00-\u9fa5]+$";// 字母数字中文

        Pattern p = Pattern.compile(yinshuzi);

        return p.matcher(str).matches();

    }

    /**
     * 验证密码长度
     */ 
    public static boolean isLegalPassword (String pwd) {
        if (pwd.length() < 6 || pwd.length() > 12) {
            return false;
        }
        return true;
    }

    /**
     * 验证手机格式
     */
    public static boolean isMobileNO(String mobiles) {

        String telRegex = "13\\d{9}|14[57]\\d{8}|15[012356789]\\d{8}|18[012356789]\\d{8}|17[0678]\\d{8}";
        if (TextUtils.isEmpty(mobiles)){
            return false;
        }
        else{
            return mobiles.matches(telRegex);
        }
    }
    /**
     * 验证身份证号码
     */
    public static String IDCardValidate(String IDStr) throws ParseException {  
        String errorInfo = "";// 记录错误信息  
        String[] ValCodeArr = { "1", "0", "X", "9", "8", "7", "6", "5", "4",  
                "3", "2" };  
        String[] Wi = { "7", "9", "10", "5", "8", "4", "2", "1", "6", "3", "7",  
                "9", "10", "5", "8", "4", "2" };  
        String Ai = "";  
        // ================ the lenth of IDCard number is 15 or 18 ================  
        if (IDStr.length() != 15 && IDStr.length() != 18) {  
            errorInfo = null;  
            return errorInfo;  
        }  
        // =======================(end)========================  

        // ================ the top 17 is numbers ================  
        if (IDStr.length() == 18) {  
            Ai = IDStr.substring(0, 17);  
        } else if (IDStr.length() == 15) {  
            Ai = IDStr.substring(0, 6) + "19" + IDStr.substring(6, 15);  
        }  
        if (isNumeric(Ai) == false) {  
            errorInfo = null;  
            return errorInfo;  
        }  
        // =======================(end)========================  

        // ================ the date of birth is legal ? ================  
        String strYear = Ai.substring(6, 10);// 年份  
        String strMonth = Ai.substring(10, 12);// 月份  
        String strDay = Ai.substring(12, 14);// 月份  
        if (isDate(strYear + "-" + strMonth + "-" + strDay) == false) {  
            errorInfo = "birthday wrong";
            return null;  
        }  
        GregorianCalendar gc = new GregorianCalendar();  
        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");  
        try {  
            if ((gc.get(Calendar.YEAR) - Integer.parseInt(strYear)) > 150  
                    || (gc.getTime().getTime() - s.parse(  
                            strYear + "-" + strMonth + "-" + strDay).getTime()) < 0) {  
                errorInfo = "birthday is not a yyyy-mm-dd format";  
                return null;  
            }  
        } catch (NumberFormatException e) {
            Log4jUtil.e(e.toString());
            e.printStackTrace();  
        } catch (java.text.ParseException e) {
            Log4jUtil.e(e.toString());
            e.printStackTrace();  
        }  
        if (Integer.parseInt(strMonth) > 12 || Integer.parseInt(strMonth) == 0) {  
            errorInfo = "the mouth day is illegal";  
            return null;  
        }  
        if (Integer.parseInt(strDay) > 31 || Integer.parseInt(strDay) == 0) {  
            errorInfo = "the day number is illegal";  
            return null;  
        }  
        // =====================(end)=====================  

        // ================ the area number is legal ? ================  
        Hashtable h = GetAreaCode();  
        if (h.get(Ai.substring(0, 2)) == null) {  
            errorInfo = "the area number is illegal";  
            return null;  
        }  
        // ==============================================   

        // ================ if the last one is number(0-9) or X ? ================  
        int TotalmulAiWi = 0;  
        for (int i = 0; i < 17; i++) {  
            TotalmulAiWi = TotalmulAiWi  
                    + Integer.parseInt(String.valueOf(Ai.charAt(i)))  
                    * Integer.parseInt(Wi[i]);  
        }  
        int modValue = TotalmulAiWi % 11;  
        String strVerifyCode = ValCodeArr[modValue];  
        Ai = Ai + strVerifyCode;  

        if (IDStr.length() == 18) {  
            if (Ai.equals(IDStr) == false) {  
                errorInfo = "the number of idcard is illegal";  
                return null;  
            }  
        } else {  
            return null;  
        }  
        // =====================(end)=====================  
        return IDStr;  
    }  
    /**  
     * area number
     *   
     * @return Hashtable object  
     */  
    @SuppressWarnings("unchecked")  
    private static Hashtable GetAreaCode() {  
        Hashtable hashtable = new Hashtable();  
        hashtable.put("11", "beijing");  
        hashtable.put("12", "tianjing");  
        hashtable.put("13", "hebei");  
        hashtable.put("14", "shanxi");  
        hashtable.put("15", "neimenggu");  
        hashtable.put("21", "niaoning");  
        hashtable.put("22", "jilin");  
        hashtable.put("23", "heilongjiang");  
        hashtable.put("31", "shanghai");  
        hashtable.put("32", "jiangsu");  
        hashtable.put("33", "zhejiang");  
        hashtable.put("34", "anhui");  
        hashtable.put("35", "fujian");  
        hashtable.put("36", "shanxi");  
        hashtable.put("37", "shandong");  
        hashtable.put("41", "henan");  
        hashtable.put("42", "hubei");  
        hashtable.put("43", "hunan");  
        hashtable.put("44", "guangdong");  
        hashtable.put("45", "guangxi");  
        hashtable.put("46", "hainan");  
        hashtable.put("50", "chongqing");  
        hashtable.put("51", "sichuan");  
        hashtable.put("52", "guizhou");  
        hashtable.put("53", "yunnan");  
        hashtable.put("54", "xizang");  
        hashtable.put("61", "shanxi");  
        hashtable.put("62", "gansu");  
        hashtable.put("63", "qinghai");  
        hashtable.put("64", "ninxia");  
        hashtable.put("65", "xizang");  
        hashtable.put("71", "taiwan");  
        hashtable.put("81", "xianggang");  
        hashtable.put("82", "aomeng");  
        hashtable.put("91", "guowai");  
        return hashtable;  
    }  
    /**  
     * if the string is number
     *   
     * @param str  
     * @return  
     */  
    public static boolean isNumeric(String str) {  
        Pattern pattern = Pattern.compile("[0-9]*");  
        Matcher isNum = pattern.matcher(str);  
        if (isNum.matches()) {  
            return true;  
        } else {  
            return false;  
        }  
    }  

    /**  
     * if the string is like a date format
     *   
     * @param str  
     * @return  
     */  
    public static boolean isDate(String strDate) {  
        Pattern pattern = Pattern  
                .compile("^((\\d{2}(([02468][048])|([13579][26]))[\\-\\/\\s]?((((0?[13578])|(1[02]))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])))))|(\\d{2}(([02468][1235679])|([13579][01345789]))[\\-\\/\\s]?((((0?[13578])|(1[02]))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\\-\\/\\s]?((0?[1-9])|(1[0-9])|(2[0-8]))))))(\\s(((0?[0-9])|([1-2][0-3]))\\:([0-5]?[0-9])((\\s)|(\\:([0-5]?[0-9])))))?$");  
        Matcher m = pattern.matcher(strDate);  
        if (m.matches()) {  
            return true;  
        } else {  
            return false;  
        }  
    }  
    /**
     * 验证邮箱
     * @param email
     * @return
     */
    public static boolean checkEmail(String email){
        boolean flag = false;
        try{
            String check = "^([a-z0-9A-Z]+[-|_|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";
            Pattern regex = Pattern.compile(check);
            Matcher matcher = regex.matcher(email);
            flag = matcher.matches();
        }catch(Exception e){
            Log4jUtil.e(e.toString());
            flag = false;
        }
        return flag;
    }
}
