package com.fablesoft.projectdatacollect.util;

import java.io.ByteArrayOutputStream;
import java.io.File;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

public class BitmapUtil extends Activity {

	
	/**
     * 根据View(主要是ImageView)的宽和高判断是否需要缩放
     * @param viewWidth
     * @param viewHeight
     * @return
     */
    public static boolean isNeedScale(File file, int viewWidth, int viewHeight){
        BitmapFactory.Options options = new BitmapFactory.Options();
        //设置为true,表示解析Bitmap对象，该对象不占内存
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(file.getPath(), options);
        int bitmapWidth = options.outWidth;
        int bitmapHeight = options.outHeight;
        if(viewWidth >= bitmapWidth && viewHeight >= bitmapHeight){
        	return false;
        }else{
        	if(file.length() < 1024 * 1024){
        		return false;
        	}else{
        		return true;
        	}
        }
    }
	
	/**
     * 根据View(主要是ImageView)的宽和高来获取图片的缩略图
     * @param path
     * @param viewWidth
     * @param viewHeight
     * @return
     */
    public static Bitmap decodeThumbBitmapForFile(String path, int viewWidth, int viewHeight){
        BitmapFactory.Options options = new BitmapFactory.Options();
        //设置为true,表示解析Bitmap对象，该对象不占内存
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);
        int height = options.outHeight;
        int width = options.outWidth;
        //设置缩放比例
        int scale = computeScale(options, viewWidth, viewHeight);
//        options.inSampleSize = computeScale(options, viewWidth, viewHeight);
        if(scale == 0){
        	scale = 1;
        }
        options.outHeight = height / scale;
        options.outWidth = width / scale;
        options.inSampleSize = scale;
        //设置为false,解析Bitmap对象加入到内存中
        options.inJustDecodeBounds = false;
        
        return BitmapFactory.decodeFile(path, options);
    }
	
	/**
     * 根据View(主要是ImageView)的宽和高来计算Bitmap缩放比例。默认不缩放
     * @param options
     */
    @SuppressLint("NewApi")
	private static int computeScale(BitmapFactory.Options options, int viewWidth, int viewHeight){
        int inSampleSize = 1;
        int bitmapWidth = options.outWidth;
        int bitmapHeight = options.outHeight;
        if(bitmapWidth > 4096 || bitmapHeight > 4096){
	    	int maxMemory = (int) (Runtime.getRuntime().maxMemory());
	    	if(maxMemory >= (512 * 1024 * 1024)){
	    		inSampleSize = 2;
	    	}else if(maxMemory <= (256 * 1024 * 1024)){
	    		inSampleSize = 4;
	    	}
        }
        if(viewWidth == 0 || viewHeight == 0){
        	Log.i("luzx", "inSampleSize1:" + inSampleSize);
            return inSampleSize;
        }
        //假如Bitmap的宽度或高度大于我们设定图片的View的宽高，则计算缩放比例
        if(bitmapWidth > viewWidth || bitmapHeight > viewHeight){
        	Log.i("luzx", "inSampleSize2:" + inSampleSize);
            int widthScale = Math.round((float) bitmapWidth / (float) viewWidth);
            int heightScale = Math.round((float) bitmapHeight / (float) viewHeight);
            
            //为了保证图片不缩放变形，我们取宽高比例最小的那个
            return widthScale < heightScale ? widthScale : heightScale;
        }else{
        	Log.i("luzx", "inSampleSize3:" + inSampleSize);
        	return 1;
        }
    }

    public static byte[] bitmapToBytes (Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        return baos.toByteArray();
    }

    public static Bitmap pathToBitmap (String path) {
        return BitmapFactory.decodeFile(path);
    }

    public static byte[] pathToBytes (String path) {
        Bitmap bitmap = BitmapFactory.decodeFile(path);
        return bitmapToBytes(bitmap);
    }

    public static File pathToFile (String path) {
        return new File(path);
    }
}
