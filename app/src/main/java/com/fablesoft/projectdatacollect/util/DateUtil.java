package com.fablesoft.projectdatacollect.util;

import android.annotation.SuppressLint;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@SuppressLint("SimpleDateFormat") public class DateUtil {

	private static final String FORMAT = "yyyy-MM-dd HH:mm:ss";
	private static final String FORMAT1 = "yyyy-MM-dd";
	
	
	public static String formatTime(long time){
	   
	    SimpleDateFormat result_formate = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	    
	    Calendar current = Calendar.getInstance();
        Calendar today = Calendar.getInstance();    //今天
        
        today.set(Calendar.YEAR, current.get(Calendar.YEAR));
        today.set(Calendar.MONTH, current.get(Calendar.MONTH));
        today.set(Calendar.DAY_OF_MONTH,current.get(Calendar.DAY_OF_MONTH));
        today.set( Calendar.HOUR_OF_DAY, 0);
        today.set( Calendar.MINUTE, 0);
        today.set(Calendar.SECOND, 0);
        
        Calendar yesterday = Calendar.getInstance();    //昨天
        yesterday.set(Calendar.YEAR, current.get(Calendar.YEAR));
        yesterday.set(Calendar.MONTH, current.get(Calendar.MONTH));
        yesterday.set(Calendar.DAY_OF_MONTH,current.get(Calendar.DAY_OF_MONTH)-1);
        yesterday.set( Calendar.HOUR_OF_DAY, 0);
        yesterday.set( Calendar.MINUTE, 0);
        yesterday.set(Calendar.SECOND, 0);
        
        Date date = new Date(time);
        current.setTime(date);
        String result = result_formate.format(date);
        if(current.after(today)){
            return "今天 "+ result.split(" ")[1];
        }else if(current.before(today) && current.after(yesterday)){
            
            return "昨天 "+ result.split(" ")[1];
        }else{
            int index = result.indexOf("-")+1;
            return result.substring(index, result.length());
        }
	}
	
	public static String formatStrDate(long time){
		SimpleDateFormat result_formate = new SimpleDateFormat(FORMAT1);
		Date date = new Date(time);
        return result_formate.format(date);
	}
	
	public static String formatStrTime(long time){
		SimpleDateFormat result_formate = new SimpleDateFormat(FORMAT1);
		Date date = new Date(time);
        return result_formate.format(date);
	}
	
	public static Date getCurrentDate(){
		SimpleDateFormat result_formate = new SimpleDateFormat(FORMAT1);
		Date date = new Date();
        try {
			return result_formate.parse(result_formate.format(date));
		} catch (ParseException e) {
			Log4jUtil.e(e.toString());
			e.printStackTrace();
		}
        return date;
	}
	
	public static Date str2Date(String str) {
		return str2Date(str, null);
	}

	public static Date str2Date(String str, String format) {
		if (str == null || str.length() == 0) {
			return null;
		}
		if (format == null || format.length() == 0) {
			format = FORMAT;
		}
		Date date = null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(format);
			date = sdf.parse(str);

		} catch (Exception e) {
			Log4jUtil.e(e.toString());
			e.printStackTrace();
		}
		return date;

	}

	public static Calendar str2Calendar(String str) {
		return str2Calendar(str, null);

	}

	public static Calendar str2Calendar(String str, String format) {

		Date date = str2Date(str, format);
		if (date == null) {
			return null;
		}
		Calendar c = Calendar.getInstance();
		c.setTime(date);

		return c;

	}

	public static String date2Str(Calendar c) {// yyyy-MM-dd HH:mm:ss
		return date2Str(c, null);
	}

	public static String date2Str(Calendar c, String format) {
		if (c == null) {
			return null;
		}
		return date2Str(c.getTime(), format);
	}

	public static String date2Str(Date d) {// yyyy-MM-dd HH:mm:ss
		return date2Str(d, null);
	}

	public static String date2Str(Date d, String format) {// yyyy-MM-dd HH:mm:ss
		if (d == null) {
			return null;
		}
		if (format == null || format.length() == 0) {
			format = FORMAT;
		}
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		String s = sdf.format(d);
		return s;
	}

	public static String getCurDateStr() {
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		return c.get(Calendar.YEAR) + "-" + (c.get(Calendar.MONTH) + 1) + "-"
				+ c.get(Calendar.DAY_OF_MONTH) + "-"
				+ c.get(Calendar.HOUR_OF_DAY) + ":" + c.get(Calendar.MINUTE)
				+ ":" + c.get(Calendar.SECOND);
	}

	public static String getCurrentDateStr(){
		Calendar c = Calendar.getInstance();
        c.setTime(new Date());
		return c.get(Calendar.YEAR) + "-" + (c.get(Calendar.MONTH) + 1) + "-"
				+ c.get(Calendar.DAY_OF_MONTH);
	}

	/**
	 * 获得当前日期的字符串格式
	 * 
	 * @param format
	 * @return
	 */
	public static String getCurDateStr(String format) {
		Calendar c = Calendar.getInstance();
		return date2Str(c, format);
	}

	// 格式到秒
	public static String getMillon(long time) {

		return new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss").format(time);

	}

	// 格式到天
	public static String getDay(long time) {

		return new SimpleDateFormat("yyyy-MM-dd").format(time);

	}
	
	// 格式到毫秒
	public static String getSMillon(long time) {

		return new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS").format(time);

	}

	public static String insertStr(String srcStr, String insertStr) {
        if (srcStr.contains("长期") || srcStr.length() < 8) {
            return srcStr;
        }
        return srcStr.substring(0, 4) + insertStr + srcStr.substring(4, 6) + insertStr + srcStr.substring(6);
    }
}
