package com.fablesoft.projectdatacollect.util;

import java.util.Calendar;  

import com.fablesoft.projectdatacollect.R;

import android.app.Activity;  
import android.app.AlertDialog;  
import android.content.DialogInterface;  
import android.text.TextUtils;
import android.widget.DatePicker;  
import android.widget.DatePicker.OnDateChangedListener;  
import android.widget.LinearLayout;  
import android.widget.TextView;
import android.widget.TimePicker;  
import android.widget.TimePicker.OnTimeChangedListener;  
  
/** 
 * 日期时间选择控件 使用方法： private EditText inputDate;//需要设置的日期时间文本编辑框 private String 
 * initDateTime="2012年9月3日 14:44",//初始日期时间值 在点击事件中使用： 
 * inputDate.setOnClickListener(new OnClickListener() { 
 *  
 * @Override public void onClick(View v) { DateTimePickDialogUtil 
 *           dateTimePicKDialog=new 
 *           DateTimePickDialogUtil(SinvestigateActivity.this,initDateTime); 
 *           dateTimePicKDialog.dateTimePicKDialog(inputDate); 
 *  
 *           } }); 
 *  
 * @author 
 */  
public class DatePickDialogUtil implements OnDateChangedListener, OnTimeChangedListener {  
    private DatePicker datePicker;  
    private TimePicker timePicker;
    private AlertDialog ad;  
    private String date;
    private String time;
    private String initDateTime;  
    private Activity activity;  
  
    /** 
     * 日期时间弹出选择框构造函数 
     *  
     * @param activity 
     *            ：调用的父activity 
     * @param initDateTime 
     *            初始日期时间值，作为弹出窗口的标题和日期时间初始值 
     */  
    public DatePickDialogUtil(Activity activity, String initDateTime) {  
        this.activity = activity;  
        this.initDateTime = initDateTime;  
  
    }  
  
    public void init(DatePicker datePicker) {  
        Calendar calendar = Calendar.getInstance();  
        if (!(null == initDateTime || "".equals(initDateTime))) {  
            calendar = this.getCalendarByInintData(initDateTime);  
        } else {  
        	date =  calendar.get(Calendar.YEAR) + "-"  
                    + calendar.get(Calendar.MONTH) + "-"  
                    + calendar.get(Calendar.DATE);
        	time = calendar.get(Calendar.HOUR_OF_DAY) + ":"  
            		+ calendar.get(Calendar.MINUTE);
            initDateTime = date + time;
        }
  
        datePicker.init(calendar.get(Calendar.YEAR),  
                calendar.get(Calendar.MONTH),  
                calendar.get(Calendar.DAY_OF_MONTH), this);  
        timePicker.setCurrentHour(calendar.get(Calendar.HOUR_OF_DAY));
        timePicker.setCurrentMinute(calendar.get(Calendar.MINUTE));
        timePicker.setOnTimeChangedListener(this);
        timePicker.setIs24HourView(true);
    }  
  
    /** 
     * 弹出日期时间选择框方法 
     *  
     * @param timeText 
     *            :为需要设置的日期时间文本编辑框 
     * @return 
     */  
    public AlertDialog dateTimePicKDialog(final TextView timeText) {  
        LinearLayout dateTimeLayout = (LinearLayout) activity  
                .getLayoutInflater().inflate(R.layout.common_datetime, null);  
        datePicker = (DatePicker) dateTimeLayout.findViewById(R.id.datepicker);  
        timePicker = (TimePicker) dateTimeLayout.findViewById(R.id.timepicker);
        init(datePicker);
        ad = new AlertDialog.Builder(activity)  
                .setTitle(initDateTime)  
                .setView(dateTimeLayout)
                .setPositiveButton("确定", new DialogInterface.OnClickListener() {  
                    @Override
					public void onClick(DialogInterface dialog, int whichButton) {  
                        timeText.setText(date + " " + time);
                    }  
                })  
                .setNegativeButton("取消", new DialogInterface.OnClickListener() {  
                    @Override
					public void onClick(DialogInterface dialog, int whichButton) {
                    	if(timeText.getTag() == null){
                    		timeText.setHint("请选择 ");
                    	}
                    }
                }).show();
        onDateChanged(null, 0, 0, 0);  
        return ad;  
    }  
  
    @Override
	public void onDateChanged(DatePicker view, int year, int monthOfYear,  
            int dayOfMonth) {  
        date = year + "-" + monthOfYear + "-" + dayOfMonth;
    	ad.setTitle(date + " " + time);
    }
  
    /** 
     * 实现将初始日期时间2012年07月02日 16:45 拆分成年 月 日 时 分 秒,并赋值给calendar 
     *  
     * @param initDateTime 
     *            初始日期时间值 字符串型 
     * @return Calendar 
     */  
    private Calendar getCalendarByInintData(String initDateTime) {  
        Calendar calendar = Calendar.getInstance();  
        if(!TextUtils.isEmpty(initDateTime)){
	        // 将初始日期时间2012-07-02 16:45 拆分成年 月 日 时 分 秒  
	        String[] strs = initDateTime.split("-");
	        String yearStr = strs[0]; // 年份  
	        String monthStr  = strs[1]; // 月  
	        String dayStr  = strs[2]; // 日  
	  
	        int currentYear = Integer.valueOf(yearStr.trim()).intValue();  
	        int currentMonth = Integer.valueOf(monthStr.trim()).intValue() - 1;  
	        int currentDay = Integer.valueOf(dayStr.trim()).intValue();  
	  
	        calendar.set(currentYear, currentMonth, currentDay);
    	}
        return calendar;  
    }

	@Override
	public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
		time = hourOfDay + ":" + minute;
    	ad.setTitle(date + " " + time);
	}
}  