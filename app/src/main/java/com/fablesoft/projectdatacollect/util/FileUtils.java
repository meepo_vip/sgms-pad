package com.fablesoft.projectdatacollect.util;

import android.os.Environment;
import android.util.Log;

import java.io.BufferedOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileUtils {

    public static File byteToFile(byte[] buf, String filePath, String fileName) {
        BufferedOutputStream bos = null;
        FileOutputStream fos = null;
        File file = null;
        try {
            File dir = new File(filePath);
            if (!dir.exists()) {
                dir.mkdirs();
            }
            file = new File(filePath + File.separator + fileName);
            fos = new FileOutputStream(file);
            bos = new BufferedOutputStream(fos);
            bos.write(buf);
        } catch (Exception e) {
            Log4jUtil.e(e.toString());
            e.printStackTrace();
        } finally {
            if (bos != null) {
                try {
                    bos.close();
                } catch (IOException e) {
                    Log4jUtil.e(e.toString());
                    e.printStackTrace();
                }
            }
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    Log4jUtil.e(e.toString());
                    e.printStackTrace();
                }
            }
        }

        return file;
    }

    /**
     * 检测SD卡是否存在
     */
    public static boolean isExistSDcard() {
        return Environment.MEDIA_MOUNTED.equals(Environment
                .getExternalStorageState());
    }

    /**
     * 从指定文件夹获取文件
     *
     * @description：
     * @author ldm
     * @date 2016-4-18 上午11:42:24
     */
    public static File getAppointFile(String folderPath, String fileNmae) {
        File file = new File(getSaveFolder(folderPath).getAbsolutePath()
                + File.separator + fileNmae);
        try {
            file.createNewFile();
        } catch (IOException e) {
            Log4jUtil.e(e.toString());
            e.printStackTrace();
        }
        return file;
    }

    /**
     * 获取文件夹对象
     *
     * @return 返回SD卡下的指定文件夹对象，若文件夹不存在则创建
     */
    public static File getSaveFolder(String folderName) {
        File file = new File(Environment.getExternalStorageDirectory()
                .getAbsoluteFile()
                + File.separator
                + folderName
                + File.separator);
        file.mkdirs();
        return file;
    }

    /**
     * 文件流关闭操作
     *
     * @description：
     * @author ldm
     * @date 2016-4-18 上午11:28:07
     */
    public static void close(Closeable... closeables) {
        if (null == closeables || closeables.length <= 0) {
            return;
        }
        for (Closeable cb : closeables) {
            try {
                if (null == cb) {
                    continue;
                }
                cb.close();
            } catch (IOException e) {
                Log4jUtil.e(e.toString());
                e.printStackTrace();
            }
        }
    }

    public static void deleteFiles(String path) {
        File dir = new File(path);
        deleteFile(dir);
    }

    public static void deleteFile(File dir) {
        if (dir == null || !dir.exists() || !dir.isDirectory()){
            return;
        }
        for (File file : dir.listFiles()) {
            if (file.isFile()){
                file.delete();
            } else {
                deleteFile(file);
            }
        }
    }
}
