package com.fablesoft.projectdatacollect.util;

import android.os.Environment;

import org.apache.log4j.Level;

import java.io.File;
import java.util.Date;

import de.mindpipe.android.logging.log4j.LogConfigurator;

public class Log4jUtil {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getRootLogger();

    public static void init() {
        final LogConfigurator logConfigurator = new LogConfigurator();
        Date nowtime = new Date();
        // String needWriteMessage = myLogSdf.format(nowtime);
        //日志文件路径地址:SD卡下myc文件夹log文件夹的test文件
        String rootPath = Environment.getExternalStorageDirectory() + "/log/com.fablesoft.projectdatacollect";
        File dirFile = new File(rootPath);
        if (!dirFile.exists()) {
            dirFile.mkdirs();
        }
        String fileName = rootPath + "/exception.log";
        //设置文件名
        logConfigurator.setFileName(fileName);
        //设置root日志输出级别 默认为DEBUG
        logConfigurator.setRootLevel(Level.DEBUG);
        // 设置日志输出级别
        logConfigurator.setLevel("org.apache", Level.INFO);
        //设置 输出到日志文件的文字格式 默认 %d %-5p [%c{2}]-[%L] %m%n
        logConfigurator.setFilePattern("%d %-5p [%c{2}]-[%L] %m%n");
        //设置输出到控制台的文字格式 默认%m%n
        logConfigurator.setLogCatPattern("%m%n");
        //设置总文件大小
        logConfigurator.setMaxFileSize(1024 * 1024 * 5);
        //设置最大产生的文件个数
        logConfigurator.setMaxBackupSize(1);
        //设置所有消息是否被立刻输出 默认为true,false 不输出
        logConfigurator.setImmediateFlush(true);
        //是否本地控制台打印输出 默认为true ，false不输出
        logConfigurator.setUseLogCatAppender(true);
        //设置是否启用文件附加,默认为true。false为覆盖文件
        logConfigurator.setUseFileAppender(true);
        //设置是否重置配置文件，默认为true
        logConfigurator.setResetConfiguration(true);
        //是否显示内部初始化日志,默认为false
        logConfigurator.setInternalDebugging(false);
        logConfigurator.configure();
    }

    public static void v(String msg) {
        log.debug(buildMessage(msg));
    }

    public static void v(String msg, Throwable thr) {
        log.debug(buildMessage(msg), thr);
    }

    public static void d(String msg) {
        log.debug(buildMessage(msg));
    }

    public static void d(String msg, Throwable thr) {
        log.debug(buildMessage(msg), thr);
    }

    public static void i(String msg) {
        log.info(buildMessage(msg));
    }

    public static void i(String msg, Throwable thr) {
        log.info(buildMessage(msg), thr);
    }

    public static void w(String msg) {
        log.warn(buildMessage(msg));
    }

    public static void w(String msg, Throwable thr) {
        log.warn(buildMessage(msg), thr);
    }

    public static void w(Throwable thr) {
        log.warn(buildMessage(""), thr);
    }

    public static void e(String msg) {
        log.error(buildMessage(msg));
    }

    public static void e(String msg, Throwable thr) {
        log.error(buildMessage(msg), thr);
    }

    /**
     * 生成消息
     * @param msg
     * @return
     */
    protected static String buildMessage(String msg) {
        StackTraceElement caller = new Throwable().fillInStackTrace()
                .getStackTrace()[2];

        return new StringBuilder().append(caller.getClassName()).append(".")
                .append(caller.getMethodName()).append("(): ").append(msg)
                .toString();
    }
}