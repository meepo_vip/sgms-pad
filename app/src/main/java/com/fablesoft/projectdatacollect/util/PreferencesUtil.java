package com.fablesoft.projectdatacollect.util;

import com.fablesoft.projectdatacollect.MyApplication;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.text.TextUtils;

public class PreferencesUtil {
	
	public static final String DEFUALT_PREFERENCE_KEY = "user_info";
	public static final String APPINFO_KEY = "app_info";
	
	public static SharedPreferences getSharedPreferences(String key) {
		if(TextUtils.isEmpty(key)){
			return MyApplication.getInstance().getSharedPreferences(
	        		DEFUALT_PREFERENCE_KEY, Context.MODE_PRIVATE);
		}else{
			return MyApplication.getInstance().getSharedPreferences(
					key, Context.MODE_MULTI_PROCESS);
		}
    }
	
	@SuppressLint("CommitPrefEdits")
	public static Editor getSharedPreferencesEditor(String key) {
        SharedPreferences cCPreferences = getSharedPreferences(key);
        Editor edit = cCPreferences.edit();
        return edit;
    }
}
