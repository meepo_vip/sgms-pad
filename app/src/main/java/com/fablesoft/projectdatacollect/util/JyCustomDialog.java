package com.fablesoft.projectdatacollect.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.fablesoft.projectdatacollect.R;

public class JyCustomDialog implements OnClickListener {

    private Dialog mDialog;
    private Context mContext;

    private TextView mMsg;
    private TextView mLeftText;
    private TextView mRightText;
    private EditText editText;

    private Handler mHandler;
    Runnable cancelRunnable = new Runnable() {
        @Override
        public void run() {
            if (mDialog != null) {
                mDialog.cancel();
            }
            mHandler.removeCallbacks(cancelRunnable);
        }
    };
    Runnable r = new Runnable() {
        @Override
        public void run() {
            if (mDialog != null && !mDialog.isShowing()) {
                mDialog.show();
            }
            mHandler.removeCallbacks(r);
        }
    };
    private TextView mTitle;
    private ImageView mImage;
    private View mImageLayout;

    public JyCustomDialog(Context context) {
        this.mContext = context;
        mHandler = new Handler();
    }

    public void cancel() {
        if (mDialog != null) {
            mDialog.cancel();
            mDialog = null;
        }
    }

    public void cancelAndHideKey() {
        if (mDialog != null) {
            hideKeyBoard();
            mDialog.cancel();
            mDialog = null;
        }
    }

    public void hideKeyBoard(){
        InputMethodManager inputMethodManager = (InputMethodManager) mDialog.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(editText.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    public JyCustomDialog createTwoButtonDialog() {
        View v = View.inflate(mContext, R.layout.dialog_two_button_layout, null);// 得到加载view
        mLeftText = (TextView) v.findViewById(R.id.bt_cancel);
        mRightText = (TextView) v.findViewById(R.id.bt_confirm);
        mMsg = (TextView) v.findViewById(R.id.tv_message);
        mDialog = new Dialog(mContext, R.style.dialogstyle);// 创建自定义样式dialog

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setView(v);
        mDialog = builder.create();
        mDialog.setCancelable(true);// 可以用“返回键”取消
        return this;
    }

    public JyCustomDialog createTwoButtonWithEditTextDialog() {
        View v = View.inflate(mContext, R.layout.dialog_two_button_with_edittext_layout, null);// 得到加载view
        mLeftText = (TextView) v.findViewById(R.id.bt_cancel);
        mRightText = (TextView) v.findViewById(R.id.bt_confirm);
        mMsg = (TextView) v.findViewById(R.id.tv_message);
        editText = (EditText) v.findViewById(R.id.et_message);
        mDialog = new Dialog(mContext, R.style.dialogstyle);// 创建自定义样式dialog

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setView(v);
        mDialog = builder.create();
        mDialog.setCancelable(false);
        mDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_UNSPECIFIED);

        return this;
    }

    public JyCustomDialog setCancelable(boolean b) {
        mDialog.setCancelable(b);
        return this;
    }

    public JyCustomDialog setLeftBtnClick(OnClickListener onClick) {
        mLeftText.setOnClickListener(onClick);
        return this;
    }

    public JyCustomDialog setRightBtnClick(OnClickListener onClick) {
        mRightText.setOnClickListener(onClick);
        return this;
    }


    public JyCustomDialog setLeftBtnClick(String text, int size, int color, OnClickListener onClick) {
        mLeftText.setText(text);
        if (size > 0)
            mLeftText.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext.getResources().getDimensionPixelSize(size));
        if (color != 0)
            mLeftText.setTextColor(color);
        mLeftText.setOnClickListener(onClick);
        return this;
    }

    public JyCustomDialog setLeftBtnClick(int textid, OnClickListener onClick) {
        return setLeftBtnClick(mContext.getString(textid), 0, 0, onClick);
    }

    public JyCustomDialog setRightBtnClick(int textid, OnClickListener onClick) {
        return setRightBtnClick(mContext.getString(textid), 0, 0, onClick);
    }

    public JyCustomDialog setRightBtnClick(String text, int size, int color, OnClickListener onClick) {
        mRightText.setText(text);
        if (size > 0)
            mRightText.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext.getResources().getDimensionPixelSize(size));
        if (color != 0)
            mRightText.setTextColor(color);
        if (onClick == null) {
            mRightText.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    cancel();
                }
            });
        } else {
            mRightText.setOnClickListener(onClick);
        }
        mRightText.setVisibility(View.VISIBLE);
        return this;
    }

    public JyCustomDialog setSystemType() {
        mDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        return this;
    }

    public void show() {
        if (!isShowing()) {
            mHandler.postDelayed(r, 300);
        }
    }

    public void show(long showTime) {
        show();
        mHandler.postDelayed(cancelRunnable, showTime);
    }

    public boolean isShowing() {
        if (mDialog == null)
            return false;
        return mDialog.isShowing();
    }

    public JyCustomDialog setMessage(int stringid) {
        return setMessage(mContext.getString(stringid));
    }

    public JyCustomDialog setMessage(int stringid, int imageid) {
        return setMessage(mContext.getString(stringid), imageid);
    }

    public JyCustomDialog setMessage(String msg) {
        if (mMsg != null) {
            if (TextUtils.isEmpty(msg)) {
                mMsg.setVisibility(View.GONE);
            } else {
                mMsg.setText(msg);
                mMsg.setVisibility(View.VISIBLE);
            }
        }
        return this;
    }

    public JyCustomDialog setMessage(String msg, int imageid) {
        if (mMsg != null) {
            if (TextUtils.isEmpty(msg)) {
                mMsg.setVisibility(View.GONE);
            } else {
                mMsg.setText(msg);
                mMsg.setVisibility(View.VISIBLE);
            }
        }
        if (mImage != null) {
            if (imageid > 0) {
                mImage.setImageResource(imageid);
                mImageLayout.setVisibility(View.VISIBLE);
            } else {
                mImageLayout.setVisibility(View.GONE);
            }
        }
        return this;
    }

    public JyCustomDialog setTitle(String msg) {
        if (mTitle != null) {
            if (TextUtils.isEmpty(msg)) {
                mTitle.setVisibility(View.GONE);
            } else {
                mTitle.setText(msg);
                mTitle.setVisibility(View.VISIBLE);
            }
        }
        return this;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
//            case R.id.tv_album:
//                cancel();
//                break;
//            case R.id.tv_take:
//                cancel();
//                break;
//            default:
//                break;
        }
    }

    public TextView getMsg() {
        return mMsg;
    }

    public String getInputMsg() {
        return editText.getText().toString();
    }

    public void setCanceledOnTouchOutside(boolean canceledOnTouchOutside) {
        mDialog.setCanceledOnTouchOutside(canceledOnTouchOutside);
    }

    public void setOnKeyListener(DialogInterface.OnKeyListener onKeyListener) {
        mDialog.setOnKeyListener(onKeyListener);
    }

    public void setmRightTextColor(int color) {
        mRightText.setTextColor(color);
    }

    public JyCustomDialog setEditText(String text) {
        if (!TextUtils.isEmpty(text)) {
            editText.setText(text);
        }
        return this;
    }

}
