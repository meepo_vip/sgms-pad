package com.fablesoft.projectdatacollect.view;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.webkit.WebSettings;
import android.webkit.WebView;

public class FableWebView extends WebView {

    private static final String APP_CACAHE_DIRNAME = "/webcache";
    private JavaScriptInterface jsInterface;
    private Context mContext;

    public FableWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
        // TODO Auto-generated constructor stub
        mContext = context;
        init();
    }

    private void init() {
        // String cacheDirPath = mContext.getFilesDir().getAbsolutePath()
        // + APP_CACAHE_DIRNAME;
        String appCacheDir = mContext.getDir("cache", Context.MODE_PRIVATE)
                .getPath();
        String appDatabaseDir = mContext.getDir("database",
                Context.MODE_PRIVATE).getPath();

        jsInterface = new JavaScriptInterface(this, (Activity) mContext);

        WebSettings settings = getSettings();
        settings.setLoadWithOverviewMode(true);
         settings.setUseWideViewPort(true);
        // settings.setSupportZoom(true);
        // settings.setBuiltInZoomControls(true);
        settings.supportMultipleWindows();
        settings.setAllowFileAccess(true);
        settings.setNeedInitialFocus(true);
        settings.setDomStorageEnabled(true);
        settings.setDatabaseEnabled(true);
        settings.setGeolocationEnabled(true);
        settings.setGeolocationDatabasePath(appDatabaseDir);
        settings.setAppCacheEnabled(true);
        settings.setCacheMode(WebSettings.LOAD_DEFAULT);
        settings.setDatabasePath(appDatabaseDir);
        settings.setAppCachePath(appCacheDir);
        settings.setJavaScriptEnabled(true);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        settings.setBlockNetworkImage(false);        
        settings.setLoadsImagesAutomatically(true);
        addJavascriptInterface(jsInterface, "JsObject");

        
        /**
         * clear the cache in the webview,add by taoanran
         * */
        clearCache(true);
    }

    public JavaScriptInterface getJSInterface() {
        return jsInterface;
    }

    // public boolean judgeUrl(String mUrl){
    // String ret = mUrl;
    // BaseApplication.LOGV("tao", "judgeUrl = mUrl : " + mUrl);
    // if(mUrl.contains("?")){
    // int index = mUrl.indexOf("?");
    // ret = mUrl.substring(0, index);
    // }
    // BaseApplication.LOGV("tao", "judgeUrl = ret : " + ret);
    // BaseApplication.LOGV("tao", "this.getUrl() = ret : " + this.getUrl());
    // if (ret.equals(this.getUrl())) {
    // BaseApplication.LOGV("tao", "judgeUrl return true");
    // return true;
    // } else {
    // BaseApplication.LOGV("tao", "judgeUrl return false");
    // return false;
    // }
    // }

    public boolean judgeUrl(String url, String targetUrl) {
        String ret = url;
        if (url.contains("?")) {
            int index = url.indexOf("?");
            ret = url.substring(0, index);
        }
        if (ret.equals(targetUrl)) {
            return true;
        } else {
            return false;
        }
    }
}
