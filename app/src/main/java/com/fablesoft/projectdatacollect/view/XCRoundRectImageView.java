package com.fablesoft.projectdatacollect.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader.TileMode;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.widget.ImageView;

public class XCRoundRectImageView extends ImageView {
    private Paint mPaint;
    private RectF mRectF;
    private BitmapShader shader;
    private Bitmap bitmap;
    public XCRoundRectImageView(Context context) {  
        this(context,null);  
    }  

    public XCRoundRectImageView(Context context, AttributeSet attrs) {  
        this(context, attrs,0);  
    }  

    public XCRoundRectImageView(Context context, AttributeSet attrs, int defStyle) {  
        super(context, attrs, defStyle); 
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mRectF = new RectF();
    }  

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    	super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }
    
    /**
     * 绘制圆角矩形图片
     * @author caizhiming
     */
    @Override  
    protected void onDraw(Canvas canvas) {  

        Drawable drawable = getDrawable();  
        if (null != drawable) {
//            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();  
//            Bitmap b = getRoundBitmap(bitmap, 30);  
//            final Rect rectSrc = new Rect(0, 0, b.getWidth(), b.getHeight());  
//            final Rect rectDest = new Rect(0,0,getWidth(),getHeight());
//            paint.reset();  
//            canvas.drawBitmap(b, rectSrc, rectDest, paint);
        	if(bitmap != ((BitmapDrawable) drawable).getBitmap()){
        		bitmap = ((BitmapDrawable) drawable).getBitmap();
        		bitmap = Bitmap.createScaledBitmap(bitmap, getMeasuredWidth(), getMeasuredWidth(), true);
                shader = new BitmapShader(bitmap, TileMode.CLAMP, TileMode.CLAMP);
        	}
            mPaint.setShader(shader);
            DisplayMetrics dm = getResources().getDisplayMetrics();
            int dp2 = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 2, dm);
            int dp1 = dp2 * 3;
            mRectF = setRectFLocation(dp1, dp1, getMeasuredWidth() - dp1, getMeasuredWidth() - dp1, mRectF);
            canvas.drawRoundRect(mRectF, dp2, dp2, mPaint);
        } else {
            super.onDraw(canvas);  
        }  
    }
 
    @Override
    protected void onDetachedFromWindow() {
    	if(bitmap != null){
    		bitmap.recycle();
    		bitmap = null;
    	}
    	shader = null;
    	super.onDetachedFromWindow();
    }
    
    //设置矩形绘制区域  
    public RectF setRectFLocation(float left,float top,float right,float bottom, RectF mRectF){
        mRectF.left = left;
        mRectF.top = top;
        mRectF.right = right;
        mRectF.bottom = bottom;
        
        return mRectF;
    }
    
    /**
     * 获取圆角矩形图片方法
     * @param bitmap
     * @param roundPx,一般设置成14
     * @return Bitmap
     * @author caizhiming
     */
//    private Bitmap getRoundBitmap(Bitmap bitmap, int roundPx) {  
//        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),  
//                bitmap.getHeight(), Config.ARGB_8888);  
//        Canvas canvas = new Canvas(output);  
//
//        final int color = 0xff424242;
//
//        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());  
//        final RectF rectF = new RectF(rect);
//        paint.setAntiAlias(true);  
//        canvas.drawARGB(0, 0, 0, 0);  
//        paint.setColor(color);  
//        int x = bitmap.getWidth(); 
//
//        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
//        paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));  
//        canvas.drawBitmap(bitmap, rect, rect, paint);  
//        return output;  
//
//
//    }  
}
