package com.fablesoft.projectdatacollect.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.GridView;

/**
 * @author zhiwei.zhao
 * @project TrafficBonus
 * @Date 2014-4-18
 * @Time 下午2:52:36
 * @Description 自定义ListView
 */
public class MyGridView extends GridView {
	
	private boolean isMeasure; 
	
	public MyGridView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public MyGridView(Context context) {
		super(context);
	}

	public MyGridView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		isMeasure = true;
		int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2,
				MeasureSpec.AT_MOST);
		super.onMeasure(widthMeasureSpec, expandSpec);
	}
	
	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		isMeasure = false;
		super.onLayout(changed, l, t, r, b);
	}
	
	public boolean isMeasure(){
		return isMeasure;
	}
}