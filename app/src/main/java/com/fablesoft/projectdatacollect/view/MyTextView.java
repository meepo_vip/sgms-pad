package com.fablesoft.projectdatacollect.view;

import com.fablesoft.projectdatacollect.R;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.widget.TextView;

public class MyTextView extends TextView
{
    private int length;
    private int screenWidth;
    private int screenWidthWeight;
    private boolean baseScreenWidth = false;
    
    public MyTextView(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
    }

    public MyTextView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        DisplayMetrics dm = context.getResources().getDisplayMetrics();
        screenWidth = dm.widthPixels;
        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.MyTextView);
        length = array.getInt(R.styleable.MyTextView_length, 20);
        screenWidthWeight = array.getInt(R.styleable.MyTextView_screenWidthWeight, 1);
        baseScreenWidth = array.getBoolean(R.styleable.MyTextView_baseScreenWidth, false);
        array.recycle();
    }

    public MyTextView(Context context)
    {
        super(context);
    }

//    @Override protected void onDraw(Canvas canvas)
//    {
//        int width = getWidth();
//        float size = width / length;
//        mPaint.setTextSize(size);
//        mPaint.setColor(getCurrentTextColor());
//        FontMetrics fontMetrics = mPaint.getFontMetrics();
//        float fontHeight = fontMetrics.bottom - fontMetrics.top;
//        float baseY = (float) (0.5 * getHeight() + 0.5 * fontHeight - fontMetrics.bottom);
//        String text = getText().toString();
//        canvas.drawText(text, 0, 0, mPaint);
//    }
    
    @Override protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        float size = 0;
        if(baseScreenWidth){
            size = (screenWidth / screenWidthWeight) / length;
        }else{
            int width = getWidth();
            size = width / length;
        }
        setTextSize(TypedValue.COMPLEX_UNIT_PX, size);
    };
    
}
