package com.fablesoft.projectdatacollect.view;

import java.util.List;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.fablesoft.projectdatacollect.R;
import com.fablesoft.projectdatacollect.bean.DepartmentBean;
import com.fablesoft.projectdatacollect.bean.PositionBean;

public class SpinnerPopWindow extends PopupWindow {
    private LayoutInflater inflater;
    private ListView mListView;
    private String[] types;
    private MyAdapter mAdapter;

    public SpinnerPopWindow(Context context, String[] types, OnItemClickListener clickListener) {
        super(context);
        inflater = LayoutInflater.from(context);
        this.types = types;
        init(clickListener);
    }

    private void init(OnItemClickListener clickListener) {
        View view = inflater.inflate(R.layout.spiner_window_layout, null);
        setContentView(view);
        setWidth(LayoutParams.WRAP_CONTENT);
        setHeight(LayoutParams.WRAP_CONTENT);
        setFocusable(true);
        ColorDrawable dw = new ColorDrawable(0x00);
        setBackgroundDrawable(dw);
        mListView = (ListView) view.findViewById(R.id.listview);
        mListView.setAdapter(mAdapter = new MyAdapter());
        mListView.setOnItemClickListener(clickListener);
    }

    private class MyAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return types.length;
        }

        @Override
        public Object getItem(int position) {
            return types[position];
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = inflater.inflate(R.layout.spiner_item_layout,
                        null);
                holder.tvName = (TextView) convertView
                        .findViewById(R.id.textView);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            holder.tvName.setText(types[position]);
            return convertView;
        }
    }

    private class ViewHolder {
        private TextView tvName;
    }
}