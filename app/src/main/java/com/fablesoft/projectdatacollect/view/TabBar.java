package com.fablesoft.projectdatacollect.view;

import java.util.ArrayList;

import com.fablesoft.projectdatacollect.R;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.ColorStateList;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class TabBar extends FrameLayout {

	private Context mContext;
	private View mRoot;
	private LinearLayout container;
	private Class[] fragments;
	private int[] drawables;
	private int[] selectedDrawables;
	private int containerResId;
	private int textColorSelectorId;
	
	public TabBar(Context context, AttributeSet attrs) {
		super(context, attrs);
		mContext = context;
	}

	public TabBar(Context context) {
		super(context);
		mContext = context;
	}

	@SuppressLint("InflateParams")
	/**
	 * 
	 * @param drawables 图片点击效果的drawable数组
	 * @param selectedDrawables 选中状态的drawable数组
	 * @param texts 点击效果的drawable数组
	 * @param textColorSelectorId 字体点击效果的resId
	 * @param fragments fragment数组
	 * @param containerResId fragment容器的resId
	 */
	public void initTabBarView(int[] drawables, int[] selectedDrawables, String[] texts, int textColorSelectorId,
			Class[] fragments, int containerResId) {
		
		if(texts.length != drawables.length){
			Log.e("", "drawables's length neq texts's length");
			return;
		}
		
		if(drawables.length != fragments.length){
			Log.e("", "drawables's length neq fragments's length");
			return;
		}
		this.drawables = drawables;
		this.fragments = fragments;
		this.selectedDrawables = selectedDrawables;
		this.containerResId = containerResId;
		this.textColorSelectorId = textColorSelectorId;
		LayoutInflater inflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mRoot = inflater.inflate(R.layout.tab_bar, this);
		container = (LinearLayout) mRoot.findViewById(R.id.bar_container);
		for (int i = 0; i < drawables.length; i++) {
			View v = inflater.inflate(R.layout.bar_view, null);
			LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(android.view.ViewGroup.LayoutParams.MATCH_PARENT,
                             android.view.ViewGroup.LayoutParams.MATCH_PARENT, 1.0f);
			v.setLayoutParams(param);
			ImageView icon = (ImageView) v.findViewById(R.id.icon);
			TextView text = (TextView) v.findViewById(R.id.text);
			icon.setImageResource(drawables[i]);
			text.setText(texts[i]);
			v.setOnClickListener(mTabClickListener);
			container.addView(v, i);
		}
	}

	public void setCheckItem(int index){
		container.getChildAt(index).performClick();
	}
	
	private OnClickListener mTabClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			ColorStateList csl = getResources().getColorStateList(textColorSelectorId);
			FragmentManager fm = ((FragmentActivity) mContext)
					.getSupportFragmentManager();
			ArrayList<Fragment> fragmentList = new ArrayList<Fragment>();
			for (int i = 0; i < fragments.length; i++) {
				fragmentList.add(fm.findFragmentByTag("fragment" + i));
			}
			FragmentTransaction ft = fm.beginTransaction();
			Fragment f = null;
			for (int i = 0; i < container.getChildCount(); i++) {
				if (v == container.getChildAt(i)) {
					if (fragmentList.get(i) == null) {
						try {
							f = (Fragment) fragments[i].newInstance();
						} catch (InstantiationException e) {
							e.printStackTrace();
						} catch (IllegalAccessException e) {
							e.printStackTrace();
						}
						fragmentList.add(i, f);
						ft.add(containerResId, f, "fragment" + i);
					} else {
						ft.show(fragmentList.get(i));
					}
					((ImageView) ((ViewGroup) v).getChildAt(0))
							.setImageResource(selectedDrawables[i]);
					((TextView) ((ViewGroup) v).getChildAt(1)).setTextColor(csl
							.getColorForState(
									new int[] { android.R.attr.state_pressed },
									0));
				} else {
					f = fragmentList.get(i);
					if (f != null) {
						ft.hide(f);
					}
					((ImageView) ((ViewGroup) container.getChildAt(i))
							.getChildAt(0)).setImageResource(drawables[i]);
					((TextView) ((ViewGroup) container.getChildAt(i))
							.getChildAt(1)).setTextColor(csl);
				}
			}
			ft.commit();
		}
	};
}