package com.fablesoft.projectdatacollect.view;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import com.fablesoft.projectdatacollect.MyApplication;
import com.fablesoft.projectdatacollect.ui.BaseWebPageActivity;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.CookieManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.widget.Toast;

public class JavaScriptInterface {
    private final static String TAG = "JavaScriptInterface";
    private Activity mActivity;
    private String hdnr;

    public JavaScriptInterface(WebView webView, Activity activity) {
        mActivity = activity;
    }
    
    //显示提示框,用于H5显示一些提示信息
    @JavascriptInterface
    public void showThoast(String text) { 
        Toast.makeText(mActivity, text, Toast.LENGTH_SHORT).show();
    }
    
//    @JavascriptInterface
//    public String getCookieStr() {
//    	return MyApplication.getInstance().getSessionId();
//    }
    
    //显示加载框
    @JavascriptInterface
    public void showLoadingDialog(final String text) {
    	final BaseWebPageActivity webPageActivity = (BaseWebPageActivity)mActivity;
    	webPageActivity.runOnUiThread(new Thread(){
    		@Override
    		public void run() {
    			webPageActivity.createLoadingDialog(mActivity);
    			webPageActivity.showDialog(text);
    		}
    	});
    }
    
  	//隐藏加载框
    @JavascriptInterface
    public void dismissLoadingDialog() { 
    	final BaseWebPageActivity webPageActivity = (BaseWebPageActivity)mActivity;
    	webPageActivity.runOnUiThread(new Thread(){
    		@Override
    		public void run() {
    			webPageActivity.dismissDialog();
    		}
    	});
    }
    
   /** 成为：渠道标志+识别符来源标志+hash后的终端识别符 
    * 
    * 渠道标志为： 
    * 1，andriod（a） 
    * 
    * 识别符来源标志： 
    * 1， wifi mac地址（wifi）； 
    * 2， IMEI（imei）； 
    * 3， 序列号（sn）； 
    * 4， id：随机码。若前面的都取不到时，则随机生成一个随机码，需要缓存。 
    * 
    * @param context 
    * @return 
    */  
  @JavascriptInterface
  public String getDeviceId() {  
//	  return MyApplication.getInstance().getDeviceId();
	  return android.os.Build.SERIAL;
  }
    
  @JavascriptInterface
  public void setHdnr(String hdnr) {
	 this.hdnr = hdnr;
  }
  
  public String getHdnr(){
	  return hdnr;
  }
//    @JavascriptInterface
//    public void uploadImages() {
//    	final BaseWebPageActivity webPageActivity = (BaseWebPageActivity)mActivity;
//    	webPageActivity.runOnUiThread(new Thread(){
//    		@Override
//    		public void run() {
//    			webPageActivity.showDialog();
//    		}
//    	});
//    }
    
    @JavascriptInterface
    public void saveCookie(String url) {
		CookieManager cookieManager = CookieManager.getInstance();
    	// mUrl对应的所有cookie信息
    	String cookie = cookieManager.getCookie(url);
    	if(!TextUtils.isEmpty(cookie)){
    		try {
    			Log.i("luzx", "cookie:" + URLDecoder.decode(cookie, "UTF-8"));
    		} catch (UnsupportedEncodingException e2) {
    			e2.printStackTrace();
    		}
        	String[] cookies = cookie.split("\\;");
        	if(cookies != null){
        		String news_cookie = "";// 新闻cookie信息
        		String image_cookie = "";// 图片cookie信息
        		boolean hasZjxxjl = false;// 已有图片资料cookie标识
				boolean hasReadNewsId = false;// 已有已读新闻cookie标识
        		for(int i=0; i<cookies.length; i++){
        			if(!cookies[i].contains("JSESSIONID")){
        				String[] keyValue = cookies[i].split("\\=");
        				if("zjxxjl".equals(keyValue[0].trim()) && !hasZjxxjl){
        					hasZjxxjl = true;
        					image_cookie += cookies[i];
        				}
        				if("readNewsId".equals(keyValue[0].trim()) && !hasReadNewsId){
        					hasReadNewsId = true;
        					news_cookie += cookies[i];
        				}
        	    	}
            	}
        		MyApplication app = (MyApplication)mActivity.getApplication();
    			SharedPreferences sharedPreferences = mActivity.getSharedPreferences("cookie", Context.MODE_PRIVATE);
    			SharedPreferences.Editor edit = sharedPreferences.edit();
        		if(!"".equals(news_cookie)){
					edit.putString("news_cookie", news_cookie);
            		edit.commit();
        		}
        		if(!"".equals(image_cookie)){
					edit.putString("image_cookie", image_cookie);
            		edit.commit();
        		}
        	}
    	}
    }
}
