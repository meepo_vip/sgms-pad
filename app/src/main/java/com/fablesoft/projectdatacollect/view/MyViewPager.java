package com.fablesoft.projectdatacollect.view;

import android.content.Context;  
import android.support.v4.view.ViewPager;  
import android.util.AttributeSet;  
import android.view.MotionEvent;  
  
/** 
 * viewpage 和listview 相互冲突 将父view 传递到viewpage 里面 
 *  
 * 使用父类的方法 parent.requestDisallowInterceptTouchEvent(true); 
 *  
 * 当 requestDisallowInterceptTouchEvent 如果为true的时候 表示:父view 不拦截子view的touch 事件 
 *  
 * 这个方法只是改变flag   
 *  
 * @author baozi 
 *  
 */  
public class MyViewPager extends ViewPager {  
  
	private float mDownX;
	private float mDownY;
	
    public MyViewPager(Context context) {  
        super(context);  
    }  
  
    public MyViewPager(Context context, AttributeSet attrs) {  
        super(context, attrs);  
    }
    
    @Override  
    public boolean dispatchTouchEvent(MotionEvent ev) {  
        switch (ev.getAction()) {
		case MotionEvent.ACTION_DOWN:
			mDownX = ev.getX();
			mDownY = ev.getY();
			getParent().requestDisallowInterceptTouchEvent(true);
			break;
		case MotionEvent.ACTION_MOVE:
			if(Math.abs(ev.getX() - mDownX) > Math.abs(ev.getY() - mDownY)){
				getParent().requestDisallowInterceptTouchEvent(true);
			}else{
				getParent().requestDisallowInterceptTouchEvent(false);
			}
			break;
		case MotionEvent.ACTION_CANCEL:
			getParent().requestDisallowInterceptTouchEvent(false);
			break;
		}  
        return super.dispatchTouchEvent(ev);  
    }  
} 