package com.fablesoft.projectdatacollect.view;

import com.fablesoft.projectdatacollect.R;
import android.content.Context;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class IndicatorView extends RadioGroup{

	private Context mContext;
	
	public IndicatorView(Context context) {
		this(context, null);
		mContext = context;
	}
	
	public IndicatorView(Context context, AttributeSet attrs) {
		super(context, attrs);
		mContext = context;
	}
	
	public void initIndicator(int count) {
		this.setOrientation(LinearLayout.HORIZONTAL);
		removeAllViews();
		DisplayMetrics metrics  = getResources().getDisplayMetrics();
		int wh = Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, metrics));
		for (int i = 0; i < count; i++) {
			RadioButton item = new RadioButton(mContext);
			item.setButtonDrawable(R.drawable.dot);
			item.setLayoutParams(new RadioGroup.LayoutParams(wh, wh));
			item.setBackgroundDrawable(null);
			item.setClickable(false);
			this.addView(item);
		}
	}
}
