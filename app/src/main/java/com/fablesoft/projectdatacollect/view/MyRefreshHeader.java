/**
 * @file XListViewHeader.java
 * @create Apr 18, 2012 5:22:27 PM
 * @author Maxwin
 * @description XListView's header
 */
package com.fablesoft.projectdatacollect.view;

import com.fablesoft.projectdatacollect.R;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.animation.Animation;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MyRefreshHeader extends LinearLayout {
	private LinearLayout mContainer;
	private ImageView mImageView;
	private TextView mHintTextView;
	private int mState = STATE_NORMAL;
	private Animation mRotateAnimation;
	static final int ROTATION_ANIMATION_DURATION = 1200;
	static final Interpolator ANIMATION_INTERPOLATOR = new LinearInterpolator();
//	private Animation mRotateUpAnim;
//	private Animation mRotateDownAnim;
	
//	private final int ROTATE_ANIM_DURATION = 180;
	
	public final static int STATE_NORMAL = 0;
	public final static int STATE_READY = 1;
	public final static int STATE_REFRESHING = 2;
	private Paint mPaint;
	private Bitmap rotateBitmap;

	public MyRefreshHeader(Context context) {
		super(context);
		initView(context);
	}

	/**
	 * @param context
	 * @param attrs
	 */
	public MyRefreshHeader(Context context, AttributeSet attrs) {
		super(context, attrs);
		initView(context);
	}

	private void initView(Context context) {
		// 初始情况，设置下拉刷新view高度为0
		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
				android.view.ViewGroup.LayoutParams.MATCH_PARENT, 0);
		mContainer = (LinearLayout) LayoutInflater.from(context).inflate(
				R.layout.listview_refresh_header, null);
		addView(mContainer, lp);
		setGravity(Gravity.BOTTOM);
		mImageView = (ImageView)findViewById(R.id.listview_header_icon);
		mHintTextView = (TextView)findViewById(R.id.listview_header_hint_textview);
		mRotateAnimation = new RotateAnimation(0, 720, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
				0.5f);
		mRotateAnimation.setInterpolator(ANIMATION_INTERPOLATOR);
		mRotateAnimation.setDuration(ROTATION_ANIMATION_DURATION);
		mRotateAnimation.setRepeatCount(Animation.INFINITE);
		mRotateAnimation.setRepeatMode(Animation.RESTART);
		rotateBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.default_rotate);
		mPaint = new Paint();
		//设置抗锯齿,防止过多的失真
		mPaint.setAntiAlias(true);
		mPaint.setFilterBitmap(true);
	}

	protected void onPullImpl(float scaleOfLayout) {
		float angle = scaleOfLayout * 90f;
		//创建一个与bitmap一样大小的bitmap2
		Bitmap bitmap = Bitmap.createBitmap(rotateBitmap.getWidth(), rotateBitmap.getHeight(), rotateBitmap.getConfig());
		Canvas canvas = new Canvas(bitmap);
		//主要以这个对象调用旋转方法
		Matrix matrix = new Matrix();
		//以图片中心作为旋转中心旋转
		matrix.setRotate(angle, bitmap.getWidth() / 2f, bitmap.getHeight() / 2f);
		canvas.drawBitmap(rotateBitmap, matrix, mPaint);
		//将旋转后的图片设置到界面上
		mImageView.setImageBitmap(bitmap);
	}
	
	public void setState(int state) {
		if (state == mState) return ;
		switch(state){
		case STATE_NORMAL:
			mImageView.clearAnimation();
			mHintTextView.setText(R.string.listview_header_hint_normal);
			break;
		case STATE_READY:
			mHintTextView.setText(R.string.listview_header_hint_ready);
			break;
		case STATE_REFRESHING:
			mHintTextView.setText(R.string.listview_header_hint_loading);
			mImageView.startAnimation(mRotateAnimation);
			break;
		}
		mState = state;
	}
	
	
	
	public void setVisiableHeight(int height) {
		if (height < 0)
			height = 0;
		LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) mContainer
				.getLayoutParams();
		lp.height = height;
		mContainer.setLayoutParams(lp);
	}

	public int getVisiableHeight() {
		return mContainer.getHeight();
	}

}
